﻿
namespace MediESTecaIntegrationTests
{
    //public class CustomWebApplicationFactory<TStartup>
    //    : WebApplicationFactory<Startup>
    //{
    //    protected override void ConfigureWebHost(IWebHostBuilder builder)
    //    {
    //        builder.ConfigureServices(services =>
    //        {
    //            // Remove the app's ApplicationDbContext registration.
    //            var descriptor = services.SingleOrDefault(
    //                d => d.ServiceType ==
    //                     typeof(DbContextOptions<IdentityMediatecaEstContext>));

    //            if (descriptor != null)
    //            {
    //                services.Remove(descriptor);
    //            }

    //            // Add ApplicationDbContext using an in-memory database for testing.
    //            services.AddDbContext<IdentityMediatecaEstContext>(options =>
    //            {
    //                options.UseInMemoryDatabase("InMemoryDbForTesting");
    //            });

    //            // Build the service provider.
    //            var sp = services.BuildServiceProvider();

    //            // Create a scope to obtain a reference to the database
    //            // context (IdentityMediatecaEstContext).
    //            using (var scope = sp.CreateScope())
    //            {
    //                var scopedServices = scope.ServiceProvider;
    //                var db = scopedServices.GetRequiredService<IdentityMediatecaEstContext>();
    //                var logger = scopedServices
    //                    .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

    //                //var userManager = scopedServices.GetRequiredService<UserManager<Utente>>();
    //                //var roleManager = scopedServices.GetRequiredService<RoleManager<IdentityRole>>();

    //                // Ensure the database is created.
    //                db.Database.EnsureCreated();

    //                //try
    //                //{
    //                //    // Seed the database with test data.
    //                //    Utilities.InitializeDbForTests(db);
    //                //catch (Exception ex)
    //                //{
    //                //    logger.LogError(ex, $"An error occurred seeding the " +
    //                //                        "database with test messages. Error: {ex.Message}");
    //                //}
    //            }
    //        });
    //    }

    //    public void SeedData()
    //    {
    //        var host = Server?.Host;
    //        if (host == null) { throw new ArgumentNullException("host"); }
    //        using (var scope = host.Services.CreateScope())
    //        {
    //            var scopedServices = scope.ServiceProvider;
    //            var loggerFactory = scopedServices.GetRequiredService<ILoggerFactory>();

    //            var db = scopedServices.GetRequiredService<IdentityMediatecaEstContext>();
    //            var logger = scopedServices
    //                .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

    //            var userManager = scopedServices.GetRequiredService<UserManager<Utente>>();
    //            var roleManager = scopedServices.GetRequiredService<RoleManager<IdentityRole>>();

    //            try
    //            {
    //                DbInitializer.Initialize(db, userManager, roleManager).Wait();
    //            }
    //            catch (Exception ex)
    //            {
    //                logger.LogError(ex, "An error occurred seeding the DB.");
    //            }
    //        }
    //    }

    //    protected override IWebHostBuilder CreateWebHostBuilder()
    //    {
    //        string[] args = null;

    //        return WebHost.CreateDefaultBuilder(args)
    //    .UseStartup<Startup>();
    //    }
    //}
}
