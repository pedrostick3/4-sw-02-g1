﻿using GEscola;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaIntegrationTests
{
    public class UCsControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _factory;

        public UCsControllerTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        }

        [Fact]
        public async Task UCs_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the oute
            var httpResponse = await _client.GetAsync("/UCs");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Details_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/UCs/Details/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Create_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/UCs/Create");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Edit_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/UCs/Edit/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Delete_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/UCs/Delete/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }
    }
}
