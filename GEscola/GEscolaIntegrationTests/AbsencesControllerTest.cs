﻿using GEscola;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaIntegrationTests
{
    public class AbsencesControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _factory;

        public AbsencesControllerTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        }

        [Fact]
        public async Task Absences_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the oute
            var httpResponse = await _client.GetAsync("/Absences");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Details_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Absences/Details/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task ParentalPIN_WhenAuthenticatedUser_CanRedirectToPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Absences/ParentalPIN/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.Redirect, httpResponse.StatusCode);
            Assert.StartsWith("/Absences/Justify/1",
                httpResponse.Headers.Location.OriginalString);
        }

        [Fact]
        public async Task Justify_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Absences/Justify/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Accept_WhenAuthenticatedUser_CanRedirectToPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Absences/Accept/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.Redirect, httpResponse.StatusCode);
            Assert.StartsWith("/Absences",
                httpResponse.Headers.Location.OriginalString);
        }

        [Fact]
        public async Task Decline_WhenAuthenticatedUser_CanRedirectToPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Absences/Decline/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.Redirect, httpResponse.StatusCode);
            Assert.StartsWith("/Absences",
                httpResponse.Headers.Location.OriginalString);
        }
    }
}
