﻿using GEscola;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaIntegrationTests
{
    public class SchoolClassesControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _factory;

        public SchoolClassesControllerTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        }

        [Fact]
        public async Task SchoolClasses_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the oute
            var httpResponse = await _client.GetAsync("/SchoolClasses");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Details_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/SchoolClasses/Details/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Create_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/SchoolClasses/Create");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Edit_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/SchoolClasses/Edit/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task RemoveStudent_WhenAuthenticatedUser_CanRedirectToPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/SchoolClasses/RemoveStudent/1?studentEmail=180221125%20estudantes%5Eips%5Ept");

            // Assert: 
            Assert.Equal(HttpStatusCode.Redirect, httpResponse.StatusCode);
            Assert.StartsWith("/SchoolClasses/Edit/1",
                httpResponse.Headers.Location.OriginalString);
        }

        [Fact]
        public async Task Delete_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/SchoolClasses/Delete/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }
    }
}
