﻿using GEscola;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaIntegrationTests
{
    public class CoursesControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _factory;

        public CoursesControllerTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        }

        [Fact]
        public async Task Courses_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the oute
            var httpResponse = await _client.GetAsync("/Courses");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Details_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Courses/Details/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Create_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Courses/Create");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Edit_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Courses/Edit/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task Delete_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Courses/Delete/1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }
    }
}
