﻿using GEscola;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaIntegrationTests
{
    public class EvaluationsControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _factory;

        public EvaluationsControllerTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        }

        [Fact]
        public async Task Evaluations_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the oute
            var httpResponse = await _client.GetAsync("/Evaluations");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task InitialCreate_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Evaluations/InitialCreate");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetClassesCreate_WhenAuthenticatedUser_CanAccessPage()
        {
            UserAthentication userLogin = new UserAthentication(_client);
            userLogin.AuthenticateUser("admin@ips.pt", "123456").Wait();

            // Act: request the route
            var httpResponse = await _client.GetAsync("/Evaluations/GetClassesCreate?courseID=1");

            // Assert: 
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }
    }
}
