﻿using GEscola.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace GEscola.Data
{
    public class ApplicationDbContext : IdentityDbContext<Person>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<GEscola.Models.Course> Course { get; set; }
        public DbSet<GEscola.Models.UC> UC { get; set; }
        public DbSet<GEscola.Models.SchoolClass> SchoolClass { get; set; }
        public DbSet<GEscola.Models.Room> Room { get; set; }
        public DbSet<GEscola.Models.Evaluation> Evaluation { get; set; }
        public DbSet<GEscola.Models.Grade> Grade { get; set; }
        public DbSet<GEscola.Models.ClassRoom> ClassRoom { get; set; }
        public DbSet<GEscola.Models.Absence> Absence { get; set; }
    }
}
