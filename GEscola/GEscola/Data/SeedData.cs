﻿using GEscola.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Data
{
    public static class SeedData
    {
        public static async Task Seed(UserManager<Person> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            await SeedRolesAsync(roleManager);
            await SeedUsersAsync(userManager);
            await SeedUCsAsync(context);
            await SeedCoursesAsync(context);
            await SeedSchoolClassesAsync(context);
            await SeedEvaluationsAsync(context);
            await SeedGradesAsync(context);
            await SeedRoomsAsync(context);
            await SeedClassRoomsAsync(context);
            await SeedAbsenceAsync(context);
        }

        private static async Task SeedRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var adminRole = new IdentityRole("Admin");
            if (!await roleManager.RoleExistsAsync(adminRole.Name))
            {
                await roleManager.CreateAsync(adminRole);
            }

            var classDirectorRole = new IdentityRole("ClassDirector");
            if (!await roleManager.RoleExistsAsync(classDirectorRole.Name))
            {
                await roleManager.CreateAsync(classDirectorRole);
            }

            var teacherRole = new IdentityRole("Teacher");
            if (!await roleManager.RoleExistsAsync(teacherRole.Name))
            {
                await roleManager.CreateAsync(teacherRole);
            }

            var studentRole = new IdentityRole("Student");
            if (!await roleManager.RoleExistsAsync(studentRole.Name))
            {
                await roleManager.CreateAsync(studentRole);
            }
        }

        private static async Task SeedUsersAsync(UserManager<Person> userManager)
        {
            //Admin
            if (userManager.FindByEmailAsync("admin@ips.pt").Result == null)
            {
                var admin = new Person { Name = "Admin", UserName = "admin@ips.pt", Email = "admin@ips.pt", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result = await userManager.CreateAsync(admin, "123456");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "Admin"); 
                }
            }

            //Admin From Another School
            if (userManager.FindByEmailAsync("admin@ejaf.pt").Result == null)
            {
                var admin = new Person { Name = "Administrador", UserName = "admin@ejaf.pt", Email = "admin@ejaf.pt", SchoolName = "EJAF - Externato João Alberto Faria", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result = await userManager.CreateAsync(admin, "123456");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "Admin");
                }
            }

            //Student
            if (userManager.FindByNameAsync("Pedro Dionísio").Result == null)
            {
                var pessoa1 = new Person { Birthdate = new DateTime(1998, 7, 23), Name = "Pedro Dionísio", UserName = "190221120@estudantes.ips.pt", Email = "190221120@estudantes.ips.pt", SecondaryEmail = "pedrostick3@gmail.com", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true, ParentalPIN = 1234 };
                var result1 = await userManager.CreateAsync(pessoa1, "123456");
                if (result1.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa1, "Student");
                }
            }
            
            if (userManager.FindByNameAsync("Catarina Pedrosa").Result == null)
            {
                var pessoa2 = new Person { Birthdate = new DateTime(1998, 5, 5), Name = "Catarina Pedrosa", UserName = "180221125@estudantes.ips.pt", Email = "180221125@estudantes.ips.pt", SecondaryEmail = "catarinabp5@gmail.com", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true, ParentalPIN = 1234 };
                var result2 = await userManager.CreateAsync(pessoa2, "123456");
                if (result2.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa2, "Student");
                }
            }

            if (userManager.FindByNameAsync("Rodrigo Costa").Result == null)
            {
                var pessoa2 = new Person { Birthdate = new DateTime(1998, 9, 7), Name = "Rodrigo Costa", UserName = "190221125@estudantes.ips.pt", Email = "190221125@estudantes.ips.pt", SecondaryEmail = "pedrodionisio13@gmail.com", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true, ParentalPIN = 1234 };
                var result2 = await userManager.CreateAsync(pessoa2, "123456");
                if (result2.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa2, "Student");
                }
            }

            if (userManager.FindByNameAsync("Nicole Vieira").Result == null)
            {
                var pessoa2 = new Person { Birthdate = new DateTime(1998, 12, 1), Name = "Nicole Vieira", UserName = "190221118@estudantes.ips.pt", Email = "190221118@estudantes.ips.pt", SecondaryEmail = "pedrodionisio13@gmail.com", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true, ParentalPIN = 1234 };
                var result2 = await userManager.CreateAsync(pessoa2, "123456");
                if (result2.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa2, "Student");
                }
            }

            //Deleted Student
            if (userManager.FindByNameAsync("Zé Apagado").Result == null)
            {
                var pessoa2 = new Person { Name = "Zé Apagado", UserName = "apagado@estudantes.ips.pt", Email = "apagado@estudantes.ips.pt", SecondaryEmail = "pedrodionisio13@gmail.com", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true, ParentalPIN = 1234, isDeleted = true };
                var result2 = await userManager.CreateAsync(pessoa2, "123456");
                if (result2.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa2, "Student");
                }
            }

            //Student From Another School
            if (userManager.FindByNameAsync("Zé Falido").Result == null)
            {
                var pessoa2 = new Person { Name = "Zé Falido", UserName = "ze.falido@ejaf.pt", Email = "ze.falido@ejaf.pt", SecondaryEmail = "pedrodionisio13@gmail.com", SchoolName = "EJAF - Externato João Alberto Faria", LectiveYearName = "2020/2021", EmailConfirmed = true, ParentalPIN = 1234 };
                var result2 = await userManager.CreateAsync(pessoa2, "123456");
                if (result2.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa2, "Student");
                }
            }

            //Teacher
            if (userManager.FindByNameAsync("Paulo Fournier").Result == null)
            {
                var pessoa3 = new Person { Name = "Paulo Fournier", UserName = "paulo.fournier@estsetubal.ips.pt", Email = "paulo.fournier@estsetubal.ips.pt", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result3 = await userManager.CreateAsync(pessoa3, "123456");
                if (result3.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa3, "Teacher");
                }
            }

            if (userManager.FindByNameAsync("José Cordeiro").Result == null)
            {
                var pessoa4 = new Person { Name = "José Cordeiro", UserName = "jose.cordeiro@estsetubal.ips.pt", Email = "jose.cordeiro@estsetubal.ips.pt", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result4 = await userManager.CreateAsync(pessoa4, "123456");
                if (result4.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa4, "Teacher");
                }
            }

            if (userManager.FindByNameAsync("Alfredo Nabiça").Result == null)
            {
                var pessoa3 = new Person { Name = "Alfredo Nabiça", UserName = "alfredo.nabica@estsetubal.ips.pt", Email = "alfredo.nabica@estsetubal.ips.pt", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result3 = await userManager.CreateAsync(pessoa3, "123456");
                if (result3.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa3, "Teacher");
                }
            }

            //Teacher From Another School
            if (userManager.FindByNameAsync("Cristiana De Profissão").Result == null)
            {
                var pessoa3 = new Person { Name = "Cristiana De Profissão", UserName = "cristiana.profissao@ejaf.pt", Email = "cristiana.profissao@ejaf.pt", SchoolName = "EJAF - Externato João Alberto Faria", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result3 = await userManager.CreateAsync(pessoa3, "123456");
                if (result3.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa3, "Teacher");
                }
            }

            //ClassDirector
            if (userManager.FindByNameAsync("Nuno Pina").Result == null)
            {
                var pessoa4 = new Person { Name = "Nuno Pina", UserName = "nuno.pina@estsetubal.ips.pt", Email = "nuno.pina@estsetubal.ips.pt", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result4 = await userManager.CreateAsync(pessoa4, "123456");
                if (result4.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa4, "ClassDirector");
                }
            }

            if (userManager.FindByNameAsync("Maria Avestruz").Result == null)
            {
                var pessoa3 = new Person { Name = "Maria Avestruz", UserName = "maria.avestruz@estsetubal.ips.pt", Email = "maria.avestruz@estsetubal.ips.pt", SchoolName = "IPS - ESTSetúbal", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result3 = await userManager.CreateAsync(pessoa3, "123456");
                if (result3.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa3, "ClassDirector");
                }
            }

            //ClassDirector From Another School
            if (userManager.FindByNameAsync("João Ratão").Result == null)
            {
                var pessoa4 = new Person { Name = "João Ratão", UserName = "joao.ratao@ejaf.pt", Email = "joao.ratao@ejaf.pt", SchoolName = "EJAF - Externato João Alberto Faria", LectiveYearName = "2020/2021", EmailConfirmed = true };
                var result4 = await userManager.CreateAsync(pessoa4, "123456");
                if (result4.Succeeded)
                {
                    await userManager.AddToRoleAsync(pessoa4, "ClassDirector");
                }
            }
        }

        private static async Task SeedUCsAsync(ApplicationDbContext context)
        {
            var ucExists = context.UC.FirstOrDefault(u => u.Name.Equals("ESA"));
            if (ucExists == null)
            {
                var uCurricular = new UC { Name = "ESA", SchoolName = "IPS - ESTSetúbal", TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("nuno.pina@estsetubal.ips.pt")).Id, Description = "Engenharia de Software Aplicada", TeacherName = "Nuno Pina" };
                context.Add(uCurricular);
                await context.SaveChangesAsync();
            }

            ucExists = context.UC.FirstOrDefault(u => u.Name.Equals("PV"));
            if (ucExists == null)
            {
                var uCurricular = new UC { Name = "PV", SchoolName = "IPS - ESTSetúbal", TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("jose.cordeiro@estsetubal.ips.pt")).Id, Description = "Programação Visual", TeacherName = "José Cordeiro" };
                context.Add(uCurricular);
                await context.SaveChangesAsync();
            }

            ucExists = context.UC.FirstOrDefault(u => u.Name.Equals("BM"));
            if (ucExists == null)
            {
                var uCurricular = new UC { Name = "BM", SchoolName = "IPS - ESTSetúbal", TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("maria.avestruz@estsetubal.ips.pt")).Id, Description = "Biologia Marinha", TeacherName = "Maria Avestruz" };
                context.Add(uCurricular);
                await context.SaveChangesAsync();
            }

            ucExists = context.UC.FirstOrDefault(u => u.Name.Equals("PT"));
            if (ucExists == null)
            {
                var uCurricular = new UC { Name = "PT", SchoolName = "EJAF - Externato João Alberto Faria", TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("joao.ratao@ejaf.pt")).Id, Description = "Português", TeacherName = "João Ratão" };
                context.Add(uCurricular);
                await context.SaveChangesAsync();
            }
        }

        private static async Task SeedCoursesAsync(ApplicationDbContext context)
        {
            var courseExists = context.Course.FirstOrDefault(c => c.Name.Equals("LEI"));
            if (courseExists == null)
            { 
                var vCourse = new Course { Name = "LEI", Description = "Licenciatura em Engenharia Informática", CourseType = (CourseType)0, CourseDuration = 3, SchoolName = "IPS - ESTSetúbal", UCs = context.UC.FirstOrDefault(u => u.Name.Equals("ESA")).UCId.ToString() + "|" + context.UC.FirstOrDefault(u => u.Name.Equals("PV")).UCId.ToString() + "|" };
                context.Add(vCourse);
                await context.SaveChangesAsync();
            }

            courseExists = context.Course.FirstOrDefault(c => c.Name.Equals("CT"));
            if (courseExists == null)
            {
                var vCourse = new Course { Name = "CT", Description = "Curso de Ciências e Tecnologias", CourseType = (CourseType)1, CourseDuration = 3, SchoolName = "IPS - ESTSetúbal", UCs = context.UC.FirstOrDefault(u => u.Name.Equals("BM")).UCId.ToString() + "|" };
                context.Add(vCourse);
                await context.SaveChangesAsync();
            }

            courseExists = context.Course.FirstOrDefault(c => c.Name.Equals("Humanidades"));
            if (courseExists == null)
            {
                var vCourse = new Course { Name = "Humanidades", Description = "Curso de Humanidades", CourseType = (CourseType)1, CourseDuration = 3, SchoolName = "EJAF - Externato João Alberto Faria", UCs = context.UC.FirstOrDefault(u => u.Name.Equals("PT")).UCId.ToString() + "|" };
                context.Add(vCourse);
                await context.SaveChangesAsync();
            }
        }

        private static async Task SeedSchoolClassesAsync(ApplicationDbContext context)
        {
            var schoolClassExists = context.SchoolClass.FirstOrDefault(c => c.Name.Equals("SW01"));
            if (schoolClassExists == null)
            {
                var sc = new SchoolClass { SchoolName = "IPS - ESTSetúbal", Name = "SW01", LectiveYear = "2020/2021", CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId, TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("nuno.pina@estsetubal.ips.pt")).Id, Students = "190221120@estudantes.ips.pt|180221125@estudantes.ips.pt|" };
                context.Add(sc);
                await context.SaveChangesAsync();
            }

            schoolClassExists = context.SchoolClass.FirstOrDefault(c => c.Name.Equals("CT01"));
            if (schoolClassExists == null)
            {
                var sc = new SchoolClass { SchoolName = "IPS - ESTSetúbal", Name = "CT01", LectiveYear = "2020/2021", CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("CT")).CourseId, TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("maria.avestruz@estsetubal.ips.pt")).Id, Students = "190221125@estudantes.ips.pt|190221118@estudantes.ips.pt|" };
                context.Add(sc);
                await context.SaveChangesAsync();
            }

            schoolClassExists = context.SchoolClass.FirstOrDefault(c => c.Name.Equals("H1"));
            if (schoolClassExists == null)
            {
                var sc = new SchoolClass { SchoolName = "EJAF - Externato João Alberto Faria", Name = "H1", LectiveYear = "2020/2021", CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId, TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("joao.ratao@ejaf.pt")).Id, Students = "ze.falido@ejaf.pt|" };
                context.Add(sc);
                await context.SaveChangesAsync();
            }
        }

        private static async Task SeedEvaluationsAsync(ApplicationDbContext context)
        {
            var evaluationExists = context.Evaluation.FirstOrDefault(c => c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("SW01")).SchoolClassId);
            if (evaluationExists == null)
            {
                var e = new Evaluation { SchoolName = "IPS - ESTSetúbal", LectiveYear = "2020/2021", CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId, SchoolClassId = context.SchoolClass.FirstOrDefault(u => u.Name.Equals("SW01")).SchoolClassId, TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("nuno.pina@estsetubal.ips.pt")).Id };
                context.Add(e);
                await context.SaveChangesAsync();
            }

            evaluationExists = context.Evaluation.FirstOrDefault(c => c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("H1")).SchoolClassId);
            if (evaluationExists == null)
            {
                var e = new Evaluation { SchoolName = "EJAF - Externato João Alberto Faria", LectiveYear = "2020/2021", CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId, SchoolClassId = context.SchoolClass.FirstOrDefault(u => u.Name.Equals("H1")).SchoolClassId, TeacherId = context.Users.FirstOrDefault(u => u.Email.Equals("joao.ratao@ejaf.pt")).Id };
                context.Add(e);
                await context.SaveChangesAsync();
            }
        }
        private static async Task SeedGradesAsync(ApplicationDbContext context)
        {
            var evaluation = context.Evaluation.FirstOrDefault(c => c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("SW01")).SchoolClassId);
            var gradeExists = context.Grade.FirstOrDefault(c => c.EvaluationID == evaluation.EvaluationId && c.StudentID == context.Users.FirstOrDefault(u => u.Email == "190221120@estudantes.ips.pt").Id && c.UCID == context.UC.FirstOrDefault(u => u.Name == "ESA").UCId);
            if (gradeExists == null)
            {
                var g1 = new Grade { SchoolName = "IPS - ESTSetúbal", LectiveYear = "2020/2021", EvaluationID = evaluation.EvaluationId, StudentID = context.Users.FirstOrDefault(u => u.Email == "190221120@estudantes.ips.pt").Id, UCID = context.UC.FirstOrDefault(u => u.Name == "ESA").UCId, GradeNumber = 10 };
                context.Add(g1);
                var g2 = new Grade { SchoolName = "IPS - ESTSetúbal", LectiveYear = "2020/2021", EvaluationID = evaluation.EvaluationId, StudentID = context.Users.FirstOrDefault(u => u.Email == "190221120@estudantes.ips.pt").Id, UCID = context.UC.FirstOrDefault(u => u.Name == "PV").UCId, GradeNumber = 14 };
                context.Add(g2);
                var g3 = new Grade { SchoolName = "IPS - ESTSetúbal", LectiveYear = "2020/2021", EvaluationID = evaluation.EvaluationId, StudentID = context.Users.FirstOrDefault(u => u.Email == "180221125@estudantes.ips.pt").Id, UCID = context.UC.FirstOrDefault(u => u.Name == "ESA").UCId, GradeNumber = 15 };
                context.Add(g3);
                var g4 = new Grade { SchoolName = "IPS - ESTSetúbal", LectiveYear = "2020/2021", EvaluationID = evaluation.EvaluationId, StudentID = context.Users.FirstOrDefault(u => u.Email == "180221125@estudantes.ips.pt").Id, UCID = context.UC.FirstOrDefault(u => u.Name == "PV").UCId, GradeNumber = 16 };
                context.Add(g4);
                await context.SaveChangesAsync();
            }

            evaluation = context.Evaluation.FirstOrDefault(c => c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("H1")).SchoolClassId);
            gradeExists = context.Grade.FirstOrDefault(c => c.EvaluationID == evaluation.EvaluationId && c.StudentID == context.Users.FirstOrDefault(u => u.Email == "ze.falido@ejaf.pt").Id && c.UCID == context.UC.FirstOrDefault(u => u.Name == "PT").UCId);
            if (gradeExists == null)
            {
                var g1 = new Grade { SchoolName = "EJAF - Externato João Alberto Faria", LectiveYear = "2020/2021", EvaluationID = evaluation.EvaluationId, StudentID = context.Users.FirstOrDefault(u => u.Email == "ze.falido@ejaf.pt").Id, UCID = context.UC.FirstOrDefault(u => u.Name == "PT").UCId, GradeNumber = 13 };
                context.Add(g1);
                
                await context.SaveChangesAsync();
            }
        }
        private static async Task SeedRoomsAsync(ApplicationDbContext context)
        {
            var roomExists = context.Room.FirstOrDefault(c => c.Name.Equals("E212"));
            if (roomExists == null)
            {
                var r = new Room { SchoolName = "IPS - ESTSetúbal", Name = "E212", MaxCapacity = 30, Size = 10, Description = "Sala de Mesas individuais com computadores nas filas laterais" };
                context.Add(r);
                await context.SaveChangesAsync();
            }

            roomExists = context.Room.FirstOrDefault(c => c.Name.Equals("B24"));
            if (roomExists == null)
            {
                var r = new Room { SchoolName = "EJAF - Externato João Alberto Faria", Name = "B24", MaxCapacity = 30, Size = 10, Description = "Sala de Mesas individuais com computadores nas filas laterais" };
                context.Add(r);
                await context.SaveChangesAsync();
            }
        }
        
        private static async Task SeedClassRoomsAsync(ApplicationDbContext context)
        {
            var classRoomExists = context.ClassRoom.FirstOrDefault(c => c.RoomId == context.Room.FirstOrDefault(u => u.Name.Equals("E212")).RoomId && c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("SW01")).SchoolClassId);
            if (classRoomExists == null)
            {
                var cr = new ClassRoom { SchoolName = "IPS - ESTSetúbal", Date = new DateTime(2021, 4, 12, 9, 5, 32), RoomId = context.Room.FirstOrDefault(u => u.Name.Equals("E212")).RoomId, CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId, SchoolClassId = context.SchoolClass.FirstOrDefault(u => u.Name.Equals("SW01")).SchoolClassId, UCId = context.UC.FirstOrDefault(u => u.Name.Equals("ESA")).UCId, TeacherId = context.Users.FirstOrDefault(u => u.Email == "nuno.pina@estsetubal.ips.pt").Id, StudentsPlaces = "180221125@estudantes.ips.pt:A1|190221120@estudantes.ips.pt:|" };
                context.Add(cr);
                await context.SaveChangesAsync();
            }

            classRoomExists = context.ClassRoom.FirstOrDefault(c => c.RoomId == context.Room.FirstOrDefault(u => u.Name.Equals("B24")).RoomId && c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("H1")).SchoolClassId);
            if (classRoomExists == null)
            {
                var cr = new ClassRoom { SchoolName = "EJAF - Externato João Alberto Faria", Date = new DateTime(2021, 4, 13, 9, 5, 32), RoomId = context.Room.FirstOrDefault(u => u.Name.Equals("B24")).RoomId, CourseId = context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId, SchoolClassId = context.SchoolClass.FirstOrDefault(u => u.Name.Equals("H1")).SchoolClassId, UCId = context.UC.FirstOrDefault(u => u.Name.Equals("PT")).UCId, TeacherId = context.Users.FirstOrDefault(u => u.Email == "joao.ratao@ejaf.pt").Id, StudentsPlaces = "ze.falido@ejaf.pt:|" };
                context.Add(cr);
                await context.SaveChangesAsync();
            }
        }

        private static async Task SeedAbsenceAsync(ApplicationDbContext context)
        {
            var classRoom = context.ClassRoom.FirstOrDefault(c => c.RoomId == context.Room.FirstOrDefault(u => u.Name.Equals("E212")).RoomId && c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("LEI")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("SW01")).SchoolClassId);
            var absenceExists = context.Absence.FirstOrDefault(c => c.ClassRoomId == classRoom.ClassRoomId && c.StudentId == context.Users.FirstOrDefault(u => u.Email == "190221120@estudantes.ips.pt").Id);
            if (absenceExists == null)
            {
                var a = new Absence { SchoolName = "IPS - ESTSetúbal", ClassRoomId = classRoom.ClassRoomId, StudentId = context.Users.FirstOrDefault(u => u.Email == "190221120@estudantes.ips.pt").Id };
                context.Add(a);
                await context.SaveChangesAsync();
            }

            classRoom = context.ClassRoom.FirstOrDefault(c => c.RoomId == context.Room.FirstOrDefault(u => u.Name.Equals("B24")).RoomId && c.CourseId == context.Course.FirstOrDefault(u => u.Name.Equals("Humanidades")).CourseId && c.SchoolClassId == context.SchoolClass.FirstOrDefault(u => u.Name.Equals("H1")).SchoolClassId);
            absenceExists = context.Absence.FirstOrDefault(c => c.ClassRoomId == classRoom.ClassRoomId && c.StudentId == context.Users.FirstOrDefault(u => u.Email == "ze.falido@ejaf.pt").Id);
            if (absenceExists == null)
            {
                var a = new Absence { SchoolName = "EJAF - Externato João Alberto Faria", ClassRoomId = classRoom.ClassRoomId, StudentId = context.Users.FirstOrDefault(u => u.Email == "ze.falido@ejaf.pt").Id };
                context.Add(a);
                await context.SaveChangesAsync();
            }
        }
    }
}
