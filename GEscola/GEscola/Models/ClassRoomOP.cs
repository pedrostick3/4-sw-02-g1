﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class ClassRoomOP
    {
        public ClassRoom ClassRoom { get; set; }

        [Display(Name = "Lugares dos Alunos")]
        public List<string> ListStudentPlaces { get; set; }
    }
}
