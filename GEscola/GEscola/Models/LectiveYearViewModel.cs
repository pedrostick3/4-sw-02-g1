﻿using GEscola.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class LectiveYearViewModel
    {
        [Display(Name = "Nome do Novo Ano Letivo")]
        public string NewLectiveYear { get; set; }
    }
}
