﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Room
    {
        [Display(Name = "ID da Sala")]
        public int RoomId { get; set; }

        public string SchoolName { get; set; }

        [Display(Name = "Nome da Sala")]
        public string Name { get; set; }


        [Display(Name = "Lotação máxima")]
        public int MaxCapacity { get; set; }


        [Display(Name = "Tamanho da Sala (m2)")]
        public double Size { get; set; }


        [Display(Name = "Descrição")]
        public string Description { get; set; }

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;
    }
}
