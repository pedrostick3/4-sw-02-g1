﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class SchoolClass
    {
        
        [Display(Name = "ID da Turma")]
        public int SchoolClassId { get; set; }

        public string SchoolName { get; set; }

        [Display(Name = "Ano Letivo")]
        public string LectiveYear { get; set; }

        [Required]
        [Display(Name = "Nome da Turma")]
        public string Name { get; set; }

        [Display(Name = "Curso")]
        public int CourseId { get; set; }

        [Display(Name = "Diretor de Turma")]
        public string TeacherId { get; set; }

        [Display(Name = "Lista de Estudantes")]
        public string Students { get; set; }

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;
    }
}
