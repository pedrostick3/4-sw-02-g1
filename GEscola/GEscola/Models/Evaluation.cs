﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Evaluation
    {
        [Display(Name = "ID")]
        public int EvaluationId { get; set; }

        [Display(Name = "Ano Letivo")]
        public string LectiveYear { get; set; }

        public string SchoolName { get; set; }

        [Display(Name = "Turma")]
        public int SchoolClassId { get; set; }

        [Display(Name = "Diretor de Turma")]
        public string TeacherId { get; set; }

        [Display(Name = "Curso")]
        public int CourseId { get; set; }

        [Display(Name = "Ficheiro")]
        public string File { get; set; } //pdf??

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;

    }
}
