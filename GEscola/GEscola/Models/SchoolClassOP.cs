﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class SchoolClassOP
    {
        public SchoolClass SchoolClass { get; set; }

        [Display(Name = "Curso")]
        public string CourseName { get; set; }

        [Display(Name = "Diretor de Turma")]
        public string TeacherName { get; set; }
        public List<Person> StudentsList { get; set; }
        
    }
}
