﻿using GEscola.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Users
    {
        public Person person { get; set; }
        public string role { get; set; }

        
        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome da Escola")]
        public string SchoolName { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data")]
        public DateTime Birthdate { get; set; }

        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [EmailAddress]
        [Display(Name = "Email Secundário")]
        public string SecondaryEmail { get; set; }

        [Display(Name = "PIN Parental")]
        public int ParentalPIN { get; set; }

        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [StringLength(100, ErrorMessage = "A nova password deve de conter pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar password")]
        [Compare("Password", ErrorMessage = "As passwords inseridas não coincidem.")]
        public string ConfirmPassword { get; set; }
    }
}
