﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class UC
    {
        [Display(Name = "ID da Disciplina")]
        public int UCId { get; set; }

        public string SchoolName { get; set; }

        [Required]
        [Display(Name = "Nome da Disciplina")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Professor ID")]
        public string TeacherId { get; set; } //Fazer validação com a Role Teacher

        [Required]
        [Display(Name = "Descrição da Disciplina")]
        public string Description { get; set; }

        [Display(Name = "Professor Responsável")]
        public string TeacherName { get; set; } //Fazer validação com a Role Teacher

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;

    }
}
