﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password atual")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [StringLength(100, ErrorMessage = "A nova password deve de conter pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nova password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar nova password")]
        [Compare("NewPassword", ErrorMessage = "As novas passwords inseridas não coincidem.")]
        public string ConfirmPassword { get; set; }
    }
}
