﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Absence
    {
        [Display(Name = "ID da Falta")]
        public int AbsenceId { get; set; }

        public string SchoolName { get; set; }

        [Required]
        [Display(Name = "Aula")]
        public int ClassRoomId { get; set; }

        [Required]
        [Display(Name = "Aluno")]
        public string StudentId { get; set; }

        [Display(Name = "Justificação")]
        public string Justification { get; set; }

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;
    }
}
