﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class GradeOP
    {
        
        [Display(Name = "Lista das Notas")]
        public List<Grade> GradesList { get; set; }

        
    }
}
