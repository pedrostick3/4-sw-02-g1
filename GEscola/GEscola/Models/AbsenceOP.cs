﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class AbsenceOP
    {
        public Absence Absence { get; set; }
        public string Course { get; set; }
        public string SchoolClass { get; set; }
        public string Teacher { get; set; }
        public string Room { get; set; }
        public string UC { get; set; }
        public string Student { get; set; }
        public DateTime Date { get; set; }

    }
}
