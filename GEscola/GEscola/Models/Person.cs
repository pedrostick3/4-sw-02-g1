﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Person : IdentityUser
    {
        [PersonalData, Required]
        public string Name { get; set; }

        [PersonalData, Required]
        public string SchoolName { get; set; }

        [PersonalData]
        public string LectiveYearName { get; set; }

        [PersonalData]
        public string SecondaryEmail { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data")]
        public DateTime Birthdate { get; set; }

        [PersonalData]
        public int ParentalPIN { get; set; }

        [PersonalData]
        public bool isDeleted { get; set; } = false;
    }
}
