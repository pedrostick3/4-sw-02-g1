﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Grade
    {
        
        [Display(Name = "ID das Notas")]
        public int GradeId { get; set; }

        [Display(Name = "Ano Letivo")]
        public string LectiveYear { get; set; }

        public string SchoolName { get; set; }

        [Required]
        [Display(Name = "ID da Avaliação")]
        public int EvaluationID { get; set; }

        [Required]
        [Display(Name = "ID do Aluno")]
        public string StudentID { get; set; }

        [Display(Name = "ID da UC")]
        public int UCID { get; set; }

        [Range(0, 20, ErrorMessage = "O valor para {0} deve estar entre {1} e {2}.")]
        [Display(Name = "Nota")]
        public int GradeNumber { get; set; }
        
    }
}
