﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class ClassRoom
    {
        [Display(Name = "ID da Aula")]
        public int ClassRoomId { get; set; }

        public string SchoolName { get; set; }

        [Required]
        [Display(Name = "Sala")]
        public int RoomId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Curso")]
        public int CourseId { get; set; }

        [Required]
        [Display(Name = "Turma")]
        public int SchoolClassId { get; set; }

        [Required]
        [Display(Name = "Disciplina")]
        public int UCId { get; set; }

        [Required]
        [Display(Name = "Professor")]
        public string TeacherId { get; set; }

        [Display(Name = "Lista de Estudantes e respetivos lugares")]
        public string StudentsPlaces { get; set; }

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;
    }
}
