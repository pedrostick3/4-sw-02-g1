﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class CourseOP
    {
        public Course Course { get; set; }

        [Display(Name = "Disciplinas")]
        public List<Checkbox> ListCourseUCs { get; set; }
    }
}
