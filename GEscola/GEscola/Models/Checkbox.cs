﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Checkbox
    {
        public bool IsChecked { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }


    }
}
