﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Models
{
    public class Course
    {
        
        [Display(Name = "ID do Curso")]
        public int CourseId { get; set; }

        [Required]
        [Display(Name = "Nome do Curso")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Descrição do Curso")]
        public string Description { get; set; }

        [Display(Name = "Tipo de Curso")]
        [EnumDataType(typeof(CourseType))]
        public CourseType CourseType { get; set; }

        [Range(1, 5, ErrorMessage = "O valor para {0} deve estar entre {1} e {2}.")]
        [Display(Name = "Duração do Curso (em anos)")]
        public int CourseDuration { get; set; }

        [Display(Name = "Disciplinas")]
        public string UCs { get; set; }

        public string SchoolName { get; set; }

        [Display(Name = "Eliminado")]
        public bool isDeleted { get; set; } = false;

        /*
        [Display(Name = "Disciplinas")]
        public Checkbox[] CourseUCs { get; set; }
        */
    }
}
