﻿
class Index {

    constructor() {

        console.log('Hello Friend');

        this.navMenu = $('#nav');
        this.menuButton = $('#menuButton');
        this.blackContainer = $('#blackContainer');

        this.attachmentEvents();
    }

    attachmentEvents() {
        this.menuButton.addEventListener("click", this.menuEffect.bind(this));
        this.blackContainer.addEventListener("click", this.hiddenBlackContainerEffect.bind(this));
    }

    test() {
        console.log('teste1');
    }

    menuEffect() {
        this.showBlackContainer();
    }

    showBlackContainer() {
        this.blackContainer.classList.remove('hiddenContainer');
        this.navMenu.classList.add('showNav');
        this.menuButton.classList.add('hiddenButton');
    }

    hiddenBlackContainerEffect() {
        this.blackContainer.classList.add('hiddenContainer');
        this.navMenu.classList.remove('showNav');
        this.menuButton.classList.remove('hiddenButton');
    }

}

const index = new Index();