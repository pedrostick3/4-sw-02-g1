﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Identity;
using System.Diagnostics;

namespace GEscola.Controllers
{
    public class SchoolClassesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Person> _userManager;

        public SchoolClassesController(ApplicationDbContext context, UserManager<Person> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: SchoolClasses
        /// <summary>
        /// This async method of SchoolClassesController returns the Index View
        /// that shows all school classes of the logged user school, of the actual lective year.
        /// </summary>
        /// <returns>View(List of SchoolClass schoolClassesToShow)</returns>
        public async Task<IActionResult> Index()
        {
            return View(ConvertSchoolClassToOP(await _context.SchoolClass.Where(sc => sc.isDeleted == false && sc.SchoolName == Utils.getSchoolName(User, _context) && sc.LectiveYear == Utils.getLectiveYear(User, _context)).ToListAsync()));
        }

        // GET: SchoolClasses/Historic
        /// <summary>
        /// This async method of SchoolClassesController returns the Historic View
        /// that shows all school classes of the logged user school, of the previous lective years.
        /// </summary>
        /// <returns>View(List of SchoolClass schoolClassesToShow)</returns>
        public async Task<IActionResult> Historic()
        {
            return View(ConvertSchoolClassToOP(await _context.SchoolClass.Where(sc => sc.isDeleted == false && sc.SchoolName == Utils.getSchoolName(User, _context) && sc.LectiveYear != Utils.getLectiveYear(User, _context)).ToListAsync()));
        }

        // GET: SchoolClasses/Details/5
        /// <summary>
        /// This async method of SchoolClassesController returns the Details View
        /// that shows with details the school class with the given id.
        /// </summary>
        /// <param name="id">int id of the school class</param>
        /// <returns>View(SchoolClassOP schoolClassToShow)</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();
            var schoolClass = await _context.SchoolClass.FirstOrDefaultAsync(m => m.SchoolClassId == id);
            if (schoolClass == null) return NotFound();

            SchoolClassOP sc = new SchoolClassOP { SchoolClass = schoolClass, CourseName = GetCourseName(schoolClass.CourseId), TeacherName = GetTeacherName(schoolClass.TeacherId), StudentsList = SchoolClassStudentsToPersonList(schoolClass)};

            return View(sc);
        }

        /// This method of SchoolClassesController returns the list of users with
        /// Teacher role of the logged user school.
        /// </summary>
        /// <returns>List of SelectListItem usersOfRoleTeacher</returns>
        public List<SelectListItem> GetListOfTeachers()
        {
            List<SelectListItem> listTeachers = new List<SelectListItem>();
            foreach (Person p in Utils.UsersOfRole("Teacher", User, _context))
            {
                listTeachers.Add(new SelectListItem { Text = p.Name, Value = p.Id });
            }
            return listTeachers;
        }

        /// <summary>
        /// This method of SchoolClassesController returns the name of the
        /// user of the given id and of the logged user school.
        /// </summary>
        /// <param name="id">string id of the user</param>
        /// <returns>string name of the user</returns>
        public string GetTeacherName(string id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id && u.isDeleted == false && u.SchoolName == Utils.getSchoolName(User, _context));
            return (user != null) ? user.Name : "Não Atribuído";
        }

        /// <summary>
        /// This method of SchoolClassesController returns the name of the
        /// course of the given id and of the logged user school.
        /// </summary>
        /// <param name="id">string id of the course</param>
        /// <returns>string name of the course</returns>
        public string GetCourseName(int id)
        {
            var course = _context.Course.FirstOrDefault(u => u.CourseId == id && u.isDeleted == false && u.SchoolName == Utils.getSchoolName(User, _context));
            return course.Name;
        }

        /// <summary>
        /// This method of SchoolClassesController converts a List of SchoolClass model
        /// to a List of SchoolClassOP model and returns the converted List.
        /// </summary>
        /// <param name="schoolClasses">List of SchoolClass model</param>
        /// <returns>List of SchoolClassOP converted</returns>
        public List<SchoolClassOP> ConvertSchoolClassToOP(List<SchoolClass> schoolClasses)
        {
            List<SchoolClassOP> scList = new List<SchoolClassOP>();
            foreach (var sc in schoolClasses)
            {
                scList.Add(new SchoolClassOP { SchoolClass = sc, CourseName = GetCourseName(sc.CourseId), TeacherName = GetTeacherName(sc.TeacherId) });
            }
            return scList;
        }

        // GET: SchoolClasses/Create
        /// <summary>
        /// This method of SchoolClassesController returns the Create View that
        /// is used to create a school class for the logged user school.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult Create()
        {
            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            ViewBag.teachers = GetListOfTeachers();
            return View();
        }

        // POST: SchoolClasses/Create
        /// <summary>
        /// This async method of SchoolClassesController redirects to the Index View 
        /// that shows all the active school classes for the logged user school if the school class
        /// was well created, otherwise returns the same View with the respective errors. <br/>
        /// This method is used to create a school class with the given form info.
        /// </summary>
        /// <param name="schoolClass">SchoolClass form with the school class info</param>
        /// <returns>Redirects to the Index View if the school class was well created,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SchoolClass schoolClass)
        {
            if (ModelState.IsValid)
            {
                var schoolName = Utils.getSchoolName(User, _context);
                var sc = new SchoolClass { SchoolName = schoolName, Name = schoolClass.Name, CourseId = schoolClass.CourseId, LectiveYear = Utils.getLectiveYear(User, _context) };
                if (schoolClass.TeacherId != "null") sc.TeacherId = schoolClass.TeacherId;
                _context.Add(sc);
                await _context.SaveChangesAsync();
                if (schoolClass.TeacherId != "null")
                {
                    Debug.WriteLine("#11## " + schoolClass.TeacherId);
                    Debug.WriteLine("#11## " + schoolName);
                    var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == schoolClass.TeacherId && u.SchoolName == schoolName);
                    Debug.WriteLine("#22## " + user);
                    await _userManager.AddToRoleAsync(user, "ClassDirector");
                }

                return RedirectToAction(nameof(Index));
            }

            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            ViewBag.teachers = GetListOfTeachers();
            return View(schoolClass);
        }

        /// <summary>
        /// This method of SchoolClassesController converts the string of SchoolClassStudents
        /// to a List of Person, returning it.
        /// </summary>
        /// <param name="schoolClass">SchoolClass that contains the string of SchoolClassStudents</param>
        /// <returns>List of Person converted</returns>
        public List<Person> SchoolClassStudentsToPersonList(SchoolClass schoolClass)
        {
            string students = schoolClass.Students;
            if (students == null) students = "";
            List<string> stringStudentList = new List<string>(students.Split('|'));

            List<Person> allStudentsList = new List<Person>(Utils.UsersOfRole("Student", User, _context).OfType<Person>());
            List<Person> studentsList = new List<Person>();

            foreach (var studentEmail in stringStudentList)
            {
                Person student = allStudentsList.FirstOrDefault(student => student.Email.Equals(studentEmail));
                if (student != null && student.Email.Equals(studentEmail)) studentsList.Add(student);

            }
            return studentsList;
        }

        /// <summary>
        /// This method of SchoolClassesController removes the student of the 
        /// given email of string of the given SchoolClassStudents, returning 
        /// the updated string.
        /// </summary>
        /// <param name="stringList">string of school class students</param>
        /// <param name="studentEmail">string email of the student to remove</param>
        /// <returns>string of the updated SchoolClassStudents</returns>
        public string RemoveStudentOfStringList(string stringList, string studentEmail)
        {
            return stringList.Replace(studentEmail + "|", "");
        }

        /// <summary>
        /// This method of SchoolClassesController adds the student of the 
        /// given email into the string of the given SchoolClassStudents, returning 
        /// the updated string.
        /// </summary>
        /// <param name="stringList">string of school class students</param>
        /// <param name="studentEmail">string email of the student to add</param>
        /// <returns>string of the updated SchoolClassStudents</returns>
        public string AddStudentToStringList(string stringList, string studentEmail)
        {
            Person student = Utils.UsersOfRole("Student", User, _context).OfType<Person>().FirstOrDefault(student => student.Email.Equals(studentEmail));
            if (student == null) return stringList;

            if (isStudentAlreadyInSchoolClass(studentEmail)) return stringList;

            return stringList += studentEmail + "|";
        }

        /// <summary>
        /// This method of SchoolClassesController returns true if the given
        /// student email exists in a SchoolClassStudents string of a SchoolClass,
        /// otherwise returns false.
        /// </summary>
        /// <param name="studentEmail">string email of the student to find</param>
        /// <returns>true if the given student email exists otherwise returns false</returns>
        public bool isStudentAlreadyInSchoolClass(string studentEmail)
        {
            foreach(SchoolClass sc in _context.SchoolClass.Where(sc => sc.Students != null && sc.SchoolName == Utils.getSchoolName(User, _context) && sc.LectiveYear == Utils.getLectiveYear(User, _context)))
            {
                if(sc.Students.Contains(studentEmail)) return true;
            }
            return false;
        }

        // GET: SchoolClasses/Edit/5
        /// <summary>
        /// This async method of SchoolClassesController returns the Edit View
        /// that is used to edit the school class of the given id.
        /// </summary>
        /// <param name="id">string id of the school class to edit</param>
        /// <param name="oldTeacherId">dummy param</param>
        /// <returns>View(SchoolClass schoolClassToEdit)</returns>
        public async Task<IActionResult> Edit(int? id, string oldTeacherId)
        {
            if (id == null) return NotFound();
            var schoolClass = await _context.SchoolClass.FindAsync(id);
            if (schoolClass == null) return NotFound();

            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            ViewBag.teachers = GetListOfTeachers();
            ViewBag.students = SchoolClassStudentsToPersonList(schoolClass);

            return View(schoolClass);
        }

        // POST: SchoolClasses/Edit/5
        /// <summary>
        /// This async method of SchoolClassesController redirects to the Index View that shows 
        /// all the active school classes of the logged user school if the school class was well edited,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to edit a school class of the given id with the given changes.
        /// </summary>
        /// <param name="id">string id of the school class to edit</param>
        /// <param name="oldTeacherId">string of the old teacher id</param>
        /// <param name="schoolClass">ScholClass form with the schoolClassChanges</param>
        /// <returns>View depending if the school class was well edited or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string oldTeacherId, SchoolClass schoolClass)
        {
            if (id != schoolClass.SchoolClassId) return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var oldUser = _context.Users.FirstOrDefault(u => u.Id.Equals(oldTeacherId));
                    if (oldUser != null)
                    {
                        await _userManager.RemoveFromRoleAsync(oldUser, "ClassDirector");
                        await _userManager.AddToRoleAsync(oldUser, "Teacher");
                    }

                    _context.Update(schoolClass);
                    await _context.SaveChangesAsync();

                    var user = _context.Users.FirstOrDefault(u => u.Id == schoolClass.TeacherId && u.SchoolName == Utils.getSchoolName(User, _context));
                    await _userManager.AddToRoleAsync(user, "ClassDirector");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolClassExists(schoolClass.SchoolClassId)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }

            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            ViewBag.teachers = GetListOfTeachers();
            return View(schoolClass);
        }


        // POST: SchoolClasses/RemoveStudent/5?email
        /// <summary>
        /// This async method of SchoolClassesController redirects to the Edit View that is 
        /// used to edit a school class. <br/>
        /// This method is used to remove the student of the given email of a school class of the given id.
        /// </summary>
        /// <param name="id">string id of the school class</param>
        /// <param name="studentEmail">string email of the student to remove</param>
        /// <returns>Redirects to the Edit View</returns>
        public async Task<IActionResult> RemoveStudent(int? id, string studentEmail)
        {
            if (id == null) return NotFound();
            studentEmail = studentEmail.Replace(" ", "@").Replace("^", ".");

            var schoolClass = await _context.SchoolClass.FindAsync(id);

            if (schoolClass != null)
            {
                try
                {
                    schoolClass.Students = RemoveStudentOfStringList(schoolClass.Students, studentEmail);
                    _context.Update(schoolClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolClassExists(schoolClass.SchoolClassId)) return NotFound();
                    else throw;
                }
            }
            // 190221120@estudantes.ips.pt|180221125@estudantes.ips.pt|
            return RedirectToAction(nameof(Edit), new { id = schoolClass.SchoolClassId });
        }

        // POST: SchoolClasses/AddStudent/5?email
        /// <summary>
        /// This async method of SchoolClassesController redirects to the Edit View that is 
        /// used to edit a school class. <br/>
        /// This method is used to add the student of the given email of a school class of the given id.
        /// </summary>
        /// <param name="id">string id of the school class</param>
        /// <param name="studentEmail">string email of the student to add</param>
        /// <returns>Redirects to the Edit View</returns>
        [HttpPost]
        public async Task<IActionResult> AddStudent(int? id, string studentEmail)
        {
            if (id == null) return NotFound();
            studentEmail = studentEmail.Replace(" ", "@").Replace("^", ".");

            var schoolClass = await _context.SchoolClass.FindAsync(id);

            if (schoolClass != null)
            {
                try
                {
                    schoolClass.Students = AddStudentToStringList(schoolClass.Students, studentEmail);
                    _context.Update(schoolClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolClassExists(schoolClass.SchoolClassId)) return NotFound();
                    else throw;
                }
            }
            return RedirectToAction(nameof(Edit), new { id = schoolClass.SchoolClassId });
        }

        // GET: SchoolClasses/Delete/5
        /// <summary>
        /// This async method of SchoolClassesController returns the Delete View
        /// that is used to delete the school class of the given id.
        /// </summary>
        /// <param name="id">string id of the school class to delete</param>
        /// <returns>View(SchoolClass schoolClassToDelete)</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            var schoolClass = await _context.SchoolClass.FirstOrDefaultAsync(m => m.SchoolClassId == id);
            if (schoolClass == null) return NotFound();

            SchoolClassOP sc = new SchoolClassOP { SchoolClass = schoolClass, CourseName = GetCourseName(schoolClass.CourseId), TeacherName = GetTeacherName(schoolClass.TeacherId) };

            return View(sc);
        }

        // POST: SchoolClasses/Delete/5
        /// <summary>
        /// This async method of SchoolClassesController redirects to the Index View that shows 
        /// all the active school classes of the logged user school. <br/>
        /// This method is used to delete the school class of the given id.
        /// </summary>
        /// <param name="id">string id of the school class to delete</param>
        /// <returns>Redirects to the Index View</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sc = _context.SchoolClass.FirstOrDefault(s => s.SchoolClassId == id);
            var oldUser = _context.Users.FirstOrDefault(u => u.Id.Equals(sc.TeacherId));
            if (oldUser != null)
            {
                await _userManager.RemoveFromRoleAsync(oldUser, "ClassDirector");
                await _userManager.AddToRoleAsync(oldUser, "Teacher");
            }

            var schoolClass = await _context.SchoolClass.FindAsync(id);
            schoolClass.isDeleted = true;
            _context.SchoolClass.Update(schoolClass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// This method of SchoolClassesController returns true if exists a school class
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">string Id of the school class we want to find</param>
        /// <returns>true if the school class exists, otherwise returns false</returns>
        private bool SchoolClassExists(int id)
        {
            return _context.SchoolClass.Any(e => e.SchoolClassId == id && e.isDeleted == false);
        }
    }
}
