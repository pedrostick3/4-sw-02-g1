﻿using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GEscola.Controllers
{
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Person> _userManager;
        private readonly SignInManager<Person> _signInManager;
        private readonly IEmailSender _emailSender;

        public UsersController(ApplicationDbContext context, UserManager<Person> userManager, SignInManager<Person> signInManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        // GET: Users
        /// <summary>
        /// This async method of UsersController returns the Index View
        /// that shows all the active users of the logged user school.
        /// </summary>
        /// <returns>View(List of Person usersToShow)</returns>
        public async Task<ActionResult> Index()
        {
            var users = await _context.Users.Where(u => u.isDeleted == false && u.SchoolName == Utils.getSchoolName(User, _context)).ToListAsync();
            users.Remove((Person)Utils.UsersOfRole("Admin", User, _context)[0]);
            return View(users);
        }

        // GET: Users/Students
        /// <summary>
        /// This method of UsersController returns the Students View that shows
        /// all the active users with the "Student" role of the logged user school.
        /// </summary>
        /// <returns>View(List of Person usersToShow)</returns>
        public ActionResult Students()
        {
            return View(Utils.UsersOfRole("Student", User, _context));
        }

        // GET: Users/Teachers
        /// <summary>
        /// This method of UsersController returns the Teachers View that shows
        /// all the active users with the "Teacher" role of the logged user school.
        /// </summary>
        /// <returns>View(List of Person usersToShow)</returns>
        public ActionResult Teachers()
        {
            return View(Utils.UsersOfRole("Teacher", User, _context));
        }

        // GET: Users/DTs
        /// <summary>
        /// This method of UsersController returns the DTs View that shows all
        /// the active users with the "ClassDirector" role of the logged user school.
        /// </summary>
        /// <returns>View(List of Person usersToShow)</returns>
        public ActionResult DTs()
        {
            return View(Utils.UsersOfRole("ClassDirector", User, _context));
        }

        // GET: Users/Create
        /// <summary>
        /// This method of UsersController returns the Create View
        /// that is used to choose wich type of user you want create.
        /// </summary>
        /// <returns>View()</returns>
        public ActionResult Create()
        {
            return View();
        }

        // GET: Users/CreateStudent
        /// <summary>
        /// This method of UsersController returns the CreateStudent View that
        /// is used to create a user of role "Student" of the logged user school.
        /// </summary>
        /// <returns>View()</returns>
        public ActionResult CreateStudent()
        {
            return View();
        }

        // POST: Users/CreateStudent
        /// <summary>
        /// This async method of UsersController returns the CreateStudent View that
        /// is used to create a user of role "Student" of the logged user school and
        /// if the user was well created, sends a success message to show.
        /// </summary>
        /// <param name="userInfo">Users form with the user info</param>
        /// <returns>View()</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateStudent(Users userInfo)
        {
            if (ModelState.IsValid)
            {
                int pin = new Random().Next(1000, 9999);
                var user = new Person { Birthdate = userInfo.Birthdate, Name = userInfo.Name, UserName = userInfo.Email, Email = userInfo.Email, SchoolName = userInfo.SchoolName, LectiveYearName = Utils.getLectiveYear(User, _context), SecondaryEmail = userInfo.SecondaryEmail, EmailConfirmed = true, ParentalPIN = pin };
                var result = await _userManager.CreateAsync(user, userInfo.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "Student");
                    TempData["Success"] = "Estudante criado com sucesso!";

                    //enviar email para encarregado de educação
                    if (userInfo.SecondaryEmail != null) 
                    { 
                        await _emailSender.SendEmailAsync(userInfo.SecondaryEmail, "GEscola - Código Parental",
                        $"Olá, <br/>Para que possa aceder e justificar as faltas do seu educando {userInfo.Name}, é necessário aceder à plataforma GEscola e introduzir o seu código parental secreto. <br/>Código Parental: {pin} <br/>Atenciosamente,<br/>GEscola");
                    }

                    return View();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View();
        }

        // GET: Users/CreateTeacher
        /// <summary>
        /// This method of UsersController returns the CreateTeacher View
        /// that is used to create a user of role "Teacher" of the logged user school.
        /// </summary>
        /// <returns>View()</returns>
        public ActionResult CreateTeacher()
        {
            return View();
        }

        // POST: Users/CreateTeacher
        /// <summary>
        /// This async method of UsersController returns the CreateTeacher View that
        /// is used to create a user of role "Teacher" of the logged user school and
        /// if the user was well created, sends a success message to show.
        /// </summary>
        /// <param name="userInfo">Users form with the user info</param>
        /// <returns>View()</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateTeacher(Users userInfo)
        {
            if (ModelState.IsValid)
            {
                var user = new Person { Name = userInfo.Name, UserName = userInfo.Email, Email = userInfo.Email, SchoolName = userInfo.SchoolName, LectiveYearName = Utils.getLectiveYear(User, _context), EmailConfirmed = true };
                var result = await _userManager.CreateAsync(user, userInfo.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "Teacher");
                    TempData["Success"] = "Professor criado com sucesso!";
                    return View();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View();
        }

        /// <summary>
        /// This method of UsersController returns true if the role of user 
        /// of the given email is the same of the given role name and 
        /// returns false otherwise.
        /// </summary>
        /// <param name="email">string email of user</param>
        /// <param name="role">string role name</param>
        /// <returns>true if the role of user of the given email is the same of the given role name
        /// and returns false otherwise </returns>
        public bool isUserInRole(string email, string role)
        {
            return (Utils.getUserRole(email, _context).Equals(role));
        }


        // POST
        /// <summary>
        /// This async method of UsersController redirects to the Profile Action that 
        /// returns the Profile View of the Profile Controller and is used to generate 
        /// a new parental PIN for the user of the given email, sending an email with
        /// the pin alteration to the sponsor of education.
        /// </summary>
        /// <param name="email">string email of the user</param>
        /// <returns>Redirects to the Profile Action that returns the Profile View of 
        /// the Profile Controller</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GerarParentalPIN(string email)
        {
            email = email.Replace(" ", "@").Replace("^", ".");
            int pin = new Random().Next(1000, 9999);
            var user = _context.Users.FirstOrDefault(u => u.Email == email);

            if (user != null)
            {
                user.ParentalPIN = pin;

                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                    if (user.SecondaryEmail != null)
                    {
                        //enviar email para encarregado de educação
                        await _emailSender.SendEmailAsync(user.SecondaryEmail, "GEscola - Código Parental",
                        $"Olá, <br/>Para que possa aceder e justificar as faltas do seu educando {user.Name}, é necessário aceder à plataforma GEscola e introduzir o seu código parental secreto. <br/>Código Parental: {pin} <br/>Atenciosamente,<br/>GEscola");
                    }
                }
                catch (DbUpdateConcurrencyException) { throw; }
            }
            return RedirectToAction("Profile", "Profile", new { email = email.Replace("@", " ").Replace(".", "^") });
        }

        // GET: Users/NewLectiveYear
        /// <summary>
        /// This method of UsersController returns the NewLectiveYear View
        /// that is used to create a new lective year for the logged user school.
        /// </summary>
        /// <returns>View() if the logged user it's not in the "Admin" role or Redirects 
        /// to the Index Action that returns the Index View of the Home Controller</returns>
        public ActionResult NewLectiveYear()
        {
            if (!User.IsInRole("Admin")) return RedirectToAction("Index", "Home");
            return View();
        }

        // POST
        /// <summary>
        /// This async method of UsersController redirects to the Index Action that 
        /// returns the Index View of the Home Controller if it's given model is valid,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to create a new lective year for the logged user school
        /// accordingly to the given model that contains the new lective year name, sending 
        /// a confirmation email to the logged user. <br/><br/>
        /// Warning: <br/>
        /// When the given model is valid, this method deletes all Absences and ClassRooms of 
        /// the logged user shool to start the new lective year.
        /// </summary>
        /// <param name="model">LectiveYearViewModel form with the new lective year name</param>
        /// <returns>Redirects to the Index Action that returns the Index View of the Home Controller
        /// if it's given model is valid, otherwise it returns to the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewLectiveYear(LectiveYearViewModel model)
        {

            var user = Utils.getLoggedUser(User, _context);
            var schoolName = Utils.getSchoolName(User, _context);

            bool existsUser = _context.Users.Any(u => u.SchoolName == schoolName && u.LectiveYearName == model.NewLectiveYear);
            bool existsSchoolClass = _context.SchoolClass.Any(sc => sc.SchoolName == schoolName && sc.LectiveYear == model.NewLectiveYear);
            bool existsEvaluation = _context.Evaluation.Any(e => e.SchoolName == schoolName && e.LectiveYear == model.NewLectiveYear);
            bool existsGrade = _context.Grade.Any(g => g.SchoolName == schoolName && g.LectiveYear == model.NewLectiveYear);
            if (existsUser || existsSchoolClass || existsEvaluation || existsGrade)
            {
                ModelState.AddModelError(string.Empty, "Já existe um Ano Letivo com este nome, por favor escolha outro nome.");
                return View(model);
            }

            if (user != null && model.NewLectiveYear != null && model.NewLectiveYear != "")
            {
                try
                {
                    //Apagar Faltas
                    foreach (Absence a in _context.Absence.Where(a => a.SchoolName == schoolName).ToList())
                    {
                        a.isDeleted = true;
                    }

                    //Apagar Aulas
                    foreach (ClassRoom cr in _context.ClassRoom.Where(cr => cr.SchoolName == schoolName).ToList())
                    {
                        cr.isDeleted = true;
                    }

                    //Turmas, Avaliações e Notas serão passadas automáticamente para histórico

                    user.LectiveYearName = model.NewLectiveYear;
                    _context.Update(user);

                    await _context.SaveChangesAsync();

                    if (user.SecondaryEmail != null)
                    {
                        //enviar email para encarregado de educação
                        await _emailSender.SendEmailAsync(user.Email, "GEscola - Novo Ano Letivo",
                        $"Olá,<br/>O novo ano letivo \"{model.NewLectiveYear}\" foi inicializado com sucesso!<br/>Boas aulas!<br/>Atenciosamente,<br/>GEscola");
                    }
                }
                catch (DbUpdateConcurrencyException) { throw; }
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: UsersController/Edit/2cb4c4ca-91d2-40f1-969a-55b2ea34c6ae
        /// <summary>
        /// This method of UsersController returns the Edit View
        /// that is used to edit the user of the given id.
        /// </summary>
        /// <param name="id">string id of the user to edit</param>
        /// <returns>View(User userToEdit)</returns>
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null) return NotFound();
            var userClass = await _context.Users.FirstOrDefaultAsync(user => user.Id == id);
            if (userClass == null) return NotFound();

            return View(userClass);
        }

        // POST: UsersController/Edit/2cb4c4ca-91d2-40f1-969a-55b2ea34c6ae
        /// <summary>
        /// This async method of UsersController redirects to the Index View that shows 
        /// all the active users of the logged user school if the user was well edited,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to edit a user of the given id with the given changes.
        /// </summary>
        /// <param name="Id">string id of the user to edit</param>
        /// <param name="personChanges">Person form with the personChanges</param>
        /// <returns>View depending if the user was well edited or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string Id, Person personChanges)
        {
            if (Id != personChanges.Id) return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var userClass = await _context.Users.FirstOrDefaultAsync(user => user.Id == Id);

                    userClass.Name = personChanges.Name;
                    userClass.Email = personChanges.Email;
                    userClass.SchoolName = personChanges.SchoolName;

                    _context.Update(userClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(personChanges.Id)) return NotFound();
                    else throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View(personChanges);
        }

        // GET: UsersController/Delete/2cb4c4ca-91d2-40f1-969a-55b2ea34c6ae
        /// <summary>
        /// This async method of UsersController returns the Delete View
        /// that is used to delete the user of the given id.
        /// </summary>
        /// <param name="id">string id of the user to delete</param>
        /// <returns>View(User userToDelete)</returns>
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null) return NotFound();

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (user == null) return NotFound();

            return View(user);
        }

        // POST: UsersController/Delete/2cb4c4ca-91d2-40f1-969a-55b2ea34c6ae
        /// <summary>
        /// This async method of UsersController redirects to the Students View that shows 
        /// all the active users with the "Student" role of the logged user school if the user
        /// to delete is in role "Student" otherwise it redirects to the Teachers View that shows 
        /// all the active users with the "Teacher" role of the logged user school. <br/>
        /// This method is used to delete the user of the given id.
        /// </summary>
        /// <param name="id">string id of the user to delete</param>
        /// <returns>View of Students or Teachers depending on the user to delete role</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _context.Users.FindAsync(id);
            bool isStudent = isUserInRole(user.Email, "Student");
            bool isCDs = isUserInRole(user.Email, "ClassDirector");
            if (isCDs)
            {
                var schoolClass = _context.SchoolClass.FirstOrDefault(sc => sc.TeacherId.Equals(id));
                if(schoolClass != null)
                {
                    schoolClass.TeacherId = null;
                    _context.Update(schoolClass);
                    await _context.SaveChangesAsync();
                }
            }
            user.isDeleted = true;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            return (isStudent)?RedirectToAction(nameof(Students)):RedirectToAction(nameof(Teachers));
        }

        /// <summary>
        /// This method of UsersController returns true if exists a user
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="Id">string Id of the user we want to find</param>
        /// <returns>true if the user exists, otherwise returns false</returns>
        private bool UserExists(string Id)
        {
            return _context.Users.Any(e => e.Id == Id && e.isDeleted == false);
        }
    }
}
