﻿using System.Linq;
using GEscola.Data;
using System.Security.Claims;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using GEscola.Models;

namespace GEscola.Controllers
{
    public static class Utils
    {
        /// <summary>
        /// This static method returns the Person Model of the logged user.
        /// </summary>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>Person logged user</returns>
        public static Person getLoggedUser(ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            return context.Users.FirstOrDefault(u => u.Email == loggedUser.Identity.Name);
        }

        /// <summary>
        /// This static method returns the school name string of the logged user.
        /// </summary>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>string school name of user logged</returns>
        public static string getSchoolName(ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            return getLoggedUser(loggedUser, context).SchoolName;
        }

        /// <summary>
        /// This static method returns the actual lective year name string that is stored
        /// in the admin user of the logged user school.
        /// </summary>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>string of actual lective year name</returns>
        public static string getLectiveYear(ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            var userAdmin = context.Users.FirstOrDefault(u => u.SchoolName == getSchoolName(loggedUser, context) && u.Name == "Admin" );
            return userAdmin.LectiveYearName;
        }

        /// <summary>
        /// This static method returns the role name of the given user email. 
        /// </summary>
        /// <param name="email">User Email</param>
        /// <param name="context">Database Context</param>
        /// <returns>string role name of an user</returns>
        public static string getUserRole(string email, ApplicationDbContext context)
        {
            var userId = context.Users.FirstOrDefault(u => u.Email.Equals(email)).Id;
            string roleId = context.UserRoles.FirstOrDefault(ur => ur.UserId.Equals(userId)).RoleId;
            return context.Roles.FirstOrDefault(r => r.Id.Equals(roleId)).Name;
        }

        /// <summary>
        /// This static method returns an IEnumerable List that contains all the users, of 
        /// the logged user school, of the specific given role name.
        /// </summary>
        /// <param name="roleName">Role Name</param>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>IList of all the users of the given role</returns>
        public static IList UsersOfRole(string roleName, ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            string roleId = context.Roles.FirstOrDefault(role => role.Name.Equals(roleName)).Id;
            var usersIdWithRole = (from role in context.UserRoles where role.RoleId == roleId select role.UserId).ToList();
            var users = context.Users.Where(user => usersIdWithRole.Contains(user.Id) && user.SchoolName.Equals(getSchoolName(loggedUser, context)) && user.isDeleted == false).OrderBy(user => user.Name).ToList();
            return users;
        }

        /// <summary>
        /// This static method returns an IEnumerable List that contains all the users 
        /// of roles "Teacher" and "ClassDirector", of the logged user school.
        /// </summary>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>IList of all the users of "Teacher" and "ClassDirector" roles</returns>
        public static IList TeachersAndDTsUsers(ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            string teacherRoleId = context.Roles.FirstOrDefault(role => role.Name.Equals("Teacher")).Id;
            string dtRoleId = context.Roles.FirstOrDefault(role => role.Name.Equals("ClassDirector")).Id;
            var usersIdWithRole = (from role in context.UserRoles where role.RoleId == teacherRoleId || role.RoleId == dtRoleId select role.UserId).ToList();
            var users = context.Users.Where(user => usersIdWithRole.Contains(user.Id) && user.SchoolName.Equals(getSchoolName(loggedUser, context)) && user.isDeleted == false).OrderBy(user => user.Name).ToList();
            return users;
        }

        /// <summary>
        /// This static method returns a SelectListItem List, for dropdown selectors purposes,
        /// that contains all the courses of the logged user school. 
        /// </summary>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>List<SelectListItem> of all courses of the logged user school</returns>
        public static List<SelectListItem> GetListOfCourses(ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            List<SelectListItem> listCourses = new List<SelectListItem>();
            foreach (Course c in context.Course.Where(c => c.isDeleted == false && c.SchoolName == getSchoolName(loggedUser, context)).ToList())
            {
                listCourses.Add(new SelectListItem { Text = c.Name, Value = c.CourseId.ToString() });
            }
            return listCourses;
        }

        /// <summary>
        /// This static method returns a SelectListItem List, for dropdown selectors purposes,
        /// that contains the school classes of the given course id, of the logged user school.
        /// </summary>
        /// <param name="courseID">Course ID</param>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>List<SelectListItem> of school classes of the given course id of logged user school</returns>
        public static List<SelectListItem> GetListOfSchoolClasses(int courseID, ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            List<SelectListItem> listSchoolClasses = new List<SelectListItem>();
            foreach (SchoolClass sc in context.SchoolClass.Where(scs => scs.isDeleted == false && scs.Students != null && scs.CourseId == courseID && scs.SchoolName == getSchoolName(loggedUser, context) && scs.LectiveYear == getLectiveYear(loggedUser, context)).ToList())
            {
                listSchoolClasses.Add(new SelectListItem { Text = sc.Name, Value = sc.SchoolClassId.ToString() });
            }
            return listSchoolClasses;
        }

        /// <summary>
        /// This static method returns a Person List of students of the given school class id,
        /// of the logged user school and of the actual lective year.
        /// </summary>
        /// <param name="schoolClassId">School Class Id</param>
        /// <param name="loggedUser">User Logged</param>
        /// <param name="context">Database Context</param>
        /// <returns>List<Person> of students of the given school class id</returns>
        public static List<Person> GetStudentsInClass(int schoolClassId, ClaimsPrincipal loggedUser, ApplicationDbContext context)
        {
            var stringOfStudents = context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == schoolClassId && sc.SchoolName == getSchoolName(loggedUser, context) && sc.LectiveYear == getLectiveYear(loggedUser, context)).Students;

            List<Person> listOfStudents = new List<Person>();
            foreach (Person p in context.Users.Where(u => u.SchoolName == getSchoolName(loggedUser, context)).OrderBy(u => u.Name))
            {
                if (stringOfStudents.Contains(p.Email))
                {
                    listOfStudents.Add(p);
                }
            }
            return listOfStudents;
        }
    }
}
