﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace GEscola.Controllers
{
    public class ClassRoomsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;

        public ClassRoomsController(ApplicationDbContext context, IEmailSender emailSender)
        {
            _context = context;
            _emailSender = emailSender;
        }

        // GET: ClassRooms
        /// <summary>
        /// This async method of ClassRoomsController returns the Index View
        /// that shows all the active class rooms of the logged user school.
        /// </summary>
        /// <returns>View(List of ClassRoom classRoomsToShow)</returns>
        public async Task<IActionResult> Index()
        {
            var schoolName = Utils.getSchoolName(User, _context);
            List<string> courses = new List<string>();
            List<string> classes = new List<string>();
            List<string> teachers = new List<string>();
            List<string> ucs = new List<string>();
            List<string> rooms = new List<string>();
            foreach(ClassRoom cr in _context.ClassRoom.Where(c => c.isDeleted == false && c.SchoolName == schoolName).ToList())
            {
                courses.Add(_context.Course.FirstOrDefault(c => c.CourseId == cr.CourseId && c.SchoolName == schoolName).Name);
                classes.Add(_context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == cr.SchoolClassId && sc.SchoolName == schoolName).Name);
                teachers.Add(_context.Users.FirstOrDefault(u => u.Id.Equals(cr.TeacherId) && u.SchoolName == schoolName).Name);
                ucs.Add(_context.UC.FirstOrDefault(u => u.UCId == cr.UCId && u.SchoolName == schoolName).Name);
                rooms.Add(_context.Room.FirstOrDefault(r => r.RoomId == cr.RoomId && r.SchoolName == schoolName).Name);
            }
            ViewBag.courses = courses;
            ViewBag.classes = classes;
            ViewBag.teachers = teachers;
            ViewBag.ucs = ucs;
            ViewBag.rooms = rooms;
            return View(await _context.ClassRoom.Where(c => c.isDeleted == false && c.SchoolName == schoolName).ToListAsync());
        }

        // GET: ClassRooms/Details/5
        /// <summary>
        /// This async method of ClassRoomsController returns the Details View
        /// that shows with details the class room with the given id.
        /// </summary>
        /// <param name="id">int id of the class room</param>
        /// <returns>View(ClassRoom classRoomToShow)</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();
            var classRoom = await _context.ClassRoom.FirstOrDefaultAsync(m => m.ClassRoomId == id);
            if (classRoom == null) return NotFound();

            var schoolName = Utils.getSchoolName(User, _context);
            ViewBag.course = _context.Course.FirstOrDefault(c => c.CourseId == classRoom.CourseId && c.SchoolName == schoolName).Name;
            ViewBag.classe = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == classRoom.SchoolClassId && sc.SchoolName == schoolName).Name;
            ViewBag.teacher = _context.Users.FirstOrDefault(u => u.Id.Equals(classRoom.TeacherId) && u.SchoolName == schoolName).Name;
            ViewBag.uc = _context.UC.FirstOrDefault(u => u.UCId == classRoom.UCId && u.SchoolName == schoolName).Name;
            ViewBag.room = _context.Room.FirstOrDefault(r => r.RoomId == classRoom.RoomId && r.SchoolName == schoolName).Name;
            ViewBag.students = StringToList(classRoom.StudentsPlaces);

            return View(classRoom);
        }

        /// This method of ClassRoomsController returns the list of UCs,
        /// of the given course, of the logged user school.
        /// </summary>
        /// <returns>List of SelectListItem ucsOfCourse</returns>
        public List<SelectListItem> GetListOfUCs(int courseID)
        {
            var schoolName = Utils.getSchoolName(User, _context);
            List<SelectListItem> listUCs = new List<SelectListItem>();
            var course = _context.Course.FirstOrDefault(c => c.isDeleted == false && c.CourseId == courseID && c.SchoolName == schoolName);
            if (course == null) return listUCs; 

            var checkBoxList = (course.UCs == null) ? "" : course.UCs;

            List<string> list = new List<string>(checkBoxList.Split('|'));
            foreach (var uc in _context.UC.Where(c => c.isDeleted == false && c.SchoolName == schoolName).ToList())
            {
                if(list.Contains(uc.UCId.ToString()))
                {
                     listUCs.Add(new SelectListItem { Text = uc.Name, Value = uc.UCId.ToString() });
                }
            }
            return listUCs;
        }

        /// This method of ClassRoomsController returns the list of Rooms of the logged user school.
        /// </summary>
        /// <returns>List of SelectListItem roomsOfSchool</returns>
        public List<SelectListItem> GetListOfRooms()
        {
            List<SelectListItem> listRooms = new List<SelectListItem>();
            foreach (Room r in _context.Room.Where(r => r.isDeleted == false && r.SchoolName == Utils.getSchoolName(User, _context)).ToList())
            {
                listRooms.Add(new SelectListItem { Text = r.Name, Value = r.RoomId.ToString() });
            }
            return listRooms;
        }

        // GET: Evaluations/InitialCreate
        /// <summary>
        /// This method of ClassRoomsController returns the InitialCreate View
        /// that shows the form of the first step to create a new class room.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult InitialCreate()
        {
            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            return View();
        }


        // POST: Evaluations/InitialCreate
        /// <summary>
        /// This method of ClassRoomsController redirects to the GetClassesCreate View
        /// that shows the form of the second step to create a new class room.
        /// </summary>
        /// <param name="classRoom">ClassRoom class room form info</param>
        /// <returns>Redirects to the GetClassesCreate View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InitialCreate(ClassRoom classRoom)
        {
            if (classRoom.CourseId == -1)
            {
                ModelState.AddModelError(string.Empty, "É obrigatório a seleção do Curso.");
                ViewBag.courses = Utils.GetListOfCourses(User, _context);
                return View(classRoom);
            }
            return RedirectToAction(nameof(GetClassesCreate), new { courseID = classRoom.CourseId });
        }

        // GET: Evaluations/GetClassesCreate
        /// <summary>
        /// This method of ClassRoomsController returns the GetClassesCreate View
        /// that shows the form of the second step to create a new class room.
        /// </summary>
        /// <param name="courseID">int courseId to find the course to create the class room</param>
        /// <returns>View()</returns>
        public IActionResult GetClassesCreate(int courseID)
        {
            ViewBag.schoolClasses = Utils.GetListOfSchoolClasses(courseID, User, _context);
            ViewBag.courseName = _context.Course.FirstOrDefault(c => c.CourseId == courseID && c.SchoolName == Utils.getSchoolName(User, _context)).Name;
            return View();
        }


        // POST: Evaluations/GetClassesCreate
        /// <summary>
        /// This method of ClassRoomsController redirects to the Create View that is the final step
        /// to create the class room if the model form is valid, otherwise returns the GetClassesCreate View
        /// that shows the form of the second step to create a new class room, with the respective errors.
        /// </summary>
        /// <param name="classRoom">ClassRoom class room form info</param>
        /// <returns>View depending if the model form was valid or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetClassesCreate(ClassRoom classRoom)
        {
            if (classRoom.SchoolClassId == -1)
            {
                ModelState.AddModelError(string.Empty, "É obrigatório a seleção da Turma.");
                ViewBag.schoolClasses = Utils.GetListOfSchoolClasses(classRoom.CourseId, User, _context);
                return View(classRoom);
            }
            
            /* Sprint de melhoramento - x tempo após terminar aula vai deixar de existir, logo, pode ser feita esta verificação
                * else if (ModelState.IsValid)
                {
                var exists = _context.ClassRoom.FirstOrDefault(e => e.CourseId == classRoom.CourseId && e.SchoolClassId == classRoom.SchoolClassId && e.isDeleted == false);
            if (exists != null)
            {
                ModelState.AddModelError(string.Empty, "Esta aula já foi criada.");
                return RedirectToAction(nameof(Index));
            }}*/

            return RedirectToAction("Create", new { courseID = classRoom.CourseId, classID = classRoom.SchoolClassId });
        }

        // GET: ClassRooms/Create
        /// <summary>
        /// This method of ClassRoomsController returns the Create View that
        /// is used to create a class room for the logged user school.
        /// </summary>
        /// <returns>View(ClassRoomOP classRoomOP)</returns>
        public IActionResult Create(int courseID, int classID)
        {
            var schoolName = Utils.getSchoolName(User, _context);
            var courseName = _context.Course.FirstOrDefault(c => c.CourseId == courseID && c.SchoolName == schoolName).Name;
            var className = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == classID && sc.SchoolName == schoolName).Name;
            var teacherId = Utils.getLoggedUser(User, _context).Id;
            ViewBag.courseName = courseName;
            ViewBag.className = className;
            ViewBag.rooms = GetListOfRooms();
            ViewBag.ucs = GetListOfUCs(courseID);
            ViewBag.students = Utils.GetStudentsInClass(classID, User, _context);
            ClassRoomOP classRoomOP = new ClassRoomOP { ClassRoom = new ClassRoom { SchoolName = schoolName, CourseId = courseID, SchoolClassId = classID, UCId = -1, TeacherId = teacherId, StudentsPlaces = "" } };
            return View(classRoomOP);
        }

        // POST: ClassRooms/Create
        /// <summary>
        /// This async method of ClassRoomsController redirects to the Index View 
        /// that shows all the active class rooms for the logged user school if the class room
        /// was well created, otherwise returns the same View with the respective errors. <br/>
        /// This method is used to create a class room with the given form info.
        /// </summary>
        /// <param name="classRoom">ClassRoom form with the class room info</param>
        /// <returns>Redirects to the Index View if the class room was well created,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClassRoomOP classRoomOP)
        {
            var schoolName = Utils.getSchoolName(User, _context);
            if (ModelState.IsValid)
            {
                var classRoom = new ClassRoom { SchoolName = schoolName, Date = DateTime.Now, RoomId = classRoomOP.ClassRoom.RoomId, CourseId = classRoomOP.ClassRoom.CourseId, SchoolClassId = classRoomOP.ClassRoom.SchoolClassId, UCId = classRoomOP.ClassRoom.UCId, TeacherId = classRoomOP.ClassRoom.TeacherId, StudentsPlaces = StudentsPlaceListToString(classRoomOP) };
                
                _context.Add(classRoom);
                await _context.SaveChangesAsync();

                //Criação das faltas correspondentes
                var getClassRoomID = _context.ClassRoom.FirstOrDefault(cr => cr.RoomId == classRoom.RoomId && cr.CourseId == classRoom.CourseId && cr.SchoolClassId == classRoom.SchoolClassId && cr.UCId == classRoom.UCId && cr.TeacherId == classRoom.TeacherId && cr.StudentsPlaces == classRoom.StudentsPlaces && cr.SchoolName == schoolName).ClassRoomId;
                foreach(Absence a in GetAbsencesList(getClassRoomID))
                {
                    _context.Absence.Add(a);

                    var student = _context.Users.FirstOrDefault(u => u.Id.Equals(a.StudentId) && u.SchoolName == schoolName);
                    var uc = _context.UC.FirstOrDefault(uc => uc.UCId == classRoom.UCId && uc.SchoolName == schoolName);

                    if (student.SecondaryEmail != null)
                    {
                        //enviar email para encarregado de educação
                        await _emailSender.SendEmailAsync(student.SecondaryEmail, $"GEscola - Falta de {student.Name}",
                        $"Olá, <br/>O seu educando {student.Name} faltou à disciplina {uc.Name}.<br/> Por favor vá a GEscola justificar a falta.<br/><br/>Atenciosamente,<br/>GEscola");
                    }
                }
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            var courseName = _context.Course.FirstOrDefault(c => c.CourseId == classRoomOP.ClassRoom.CourseId && c.SchoolName == schoolName).Name;
            var className = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == classRoomOP.ClassRoom.SchoolClassId && sc.SchoolName == schoolName).Name;
            ViewBag.courseName = courseName;
            ViewBag.className = className;
            ViewBag.rooms = GetListOfRooms();
            ViewBag.ucs = GetListOfUCs(classRoomOP.ClassRoom.CourseId);
            ViewBag.students = Utils.GetStudentsInClass(classRoomOP.ClassRoom.SchoolClassId, User, _context);

            return View(classRoomOP);
        }

        /// This method of SchoolClassesController returns the list of abcenses the class room
        /// with the given class room id, of the logged user school.
        /// </summary>
        /// <returns>List of Abcense abcensesOfClassRoom</returns>
        public List<Absence> GetAbsencesList(int classRoomID)
        {
            var schoolName = Utils.getSchoolName(User, _context);
            List<Absence> listAbsences = new List<Absence>();
            List<Place> placesList = StringToList(_context.ClassRoom.FirstOrDefault(c => c.ClassRoomId == classRoomID && c.SchoolName == schoolName).StudentsPlaces);
            foreach (Place p in placesList)
            {                
                if (p.StudentSpot != "" && p.StudentSpot != null) continue;
                var studentID = _context.Users.FirstOrDefault(u => u.Email == p.StudentEmail && u.isDeleted == false && u.SchoolName == schoolName).Id;
                listAbsences.Add(new Absence { SchoolName = schoolName, ClassRoomId = classRoomID, StudentId = studentID });
            }
            return listAbsences;
        }

        /// <summary>
        /// This method of SchoolClassesController converts a given students places list into
        /// a string of places, returning it.
        /// </summary>
        /// <param name="placeList">string studentsPlacesList</param>
        /// <returns>List of Place studentsPlacesList</returns>
        public List<Place> StringToList(string placeList)
        {
            List<Place> listPlaces = new List<Place>();
            if (placeList == null) return listPlaces;
            foreach (string lp in placeList.Split('|'))
            {
                if (lp == null || lp == "") continue;
                string email = (lp.Split(':')[0] == null) ? "" : lp.Split(':')[0];
                string spot = (lp.Split(':')[1] == null) ? "" : lp.Split(':')[1];
                string name = _context.Users.FirstOrDefault(u => u.Email == email && u.SchoolName == Utils.getSchoolName(User, _context)).Name;
                listPlaces.Add(new Place { StudentName = (name == null) ? "" : name, StudentEmail = email, StudentSpot = spot });
            }
            return listPlaces;
        }

        /// <summary>
        /// This method of SchoolClassesController converts the students places list of the
        /// given classRoomOP to a string, returning it.
        /// </summary>
        /// <param name="classRoomOP">ClassRoomOP classRoomOP that contains the list of students places</param>
        /// <returns>string of students places list</returns>
        public string StudentsPlaceListToString(ClassRoomOP classRoomOP)
        {
            string studentPlaces = "";
            List<Person> students = Utils.GetStudentsInClass(classRoomOP.ClassRoom.SchoolClassId, User, _context);
            for (int i = 0; i< classRoomOP.ListStudentPlaces.Count; i++)
            {
                var spot = classRoomOP.ListStudentPlaces[i];
                if (spot == null || spot == "") spot = "";
                studentPlaces += students.ElementAt(i).Email + ":" + spot.ToUpper() + "|";
            }
            return studentPlaces;
        }

        /// <summary>
        /// This method of SchoolClassesController converts a given places list into
        /// a list of string of places, returning it.
        /// </summary>
        /// <param name="listPlace">List of Place studentsPlacesList</param>
        /// <returns>List of string studentsPlacesList</returns>
        public List<string> PlaceListToListStudentPlaces(List<Place> listPlace)
        {
            List<string> list = new List<string>();
            foreach (Place p in listPlace)
            {
                list.Add(p.StudentSpot);
            }
            return list;
        }

        // GET: ClassRooms/Edit/5
        /// <summary>
        /// This async method of SchoolClassesController returns the Edit View
        /// that is used to edit the class room of the given id.
        /// </summary>
        /// <param name="id">int id of the class room to edit</param>
        /// <returns>View(ClassRoomOP classRoomOP)</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            var classRoom = await _context.ClassRoom.FindAsync(id);
            if (classRoom == null) return NotFound();

            var schoolName = Utils.getSchoolName(User, _context);
            var course = await _context.Course.FirstOrDefaultAsync(c => c.CourseId == classRoom.CourseId && c.SchoolName == schoolName);
            ViewBag.course = course.Name;
            var classe = await _context.SchoolClass.FirstOrDefaultAsync(sc => sc.SchoolClassId == classRoom.SchoolClassId && sc.SchoolName == schoolName);
            ViewBag.classe = classe.Name;
            var teacher = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(classRoom.TeacherId) && u.SchoolName == schoolName);
            ViewBag.teacher = teacher.Name;
            ViewBag.ucs = GetListOfUCs(classRoom.CourseId);
            ViewBag.rooms = GetListOfRooms();
            ViewBag.students = StringToList(classRoom.StudentsPlaces);
            ClassRoomOP classRoomOP = new ClassRoomOP { ClassRoom = classRoom, ListStudentPlaces = PlaceListToListStudentPlaces(StringToList(classRoom.StudentsPlaces)) };

            return View(classRoomOP);
        }

        // POST: ClassRooms/Edit/5
        /// <summary>
        /// This async method of SchoolClassesController redirects to the Index View that shows 
        /// all the active class rooms of the logged user school if the class room was well edited,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to edit a class room of the given id with the given changes.
        /// </summary>
        /// <param name="id">int id of the class room to edit</param>
        /// <param name="classRoomOP">ClassRoomOP form with the classRoomChanges</param>
        /// <returns>View depending if the class room was well edited or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ClassRoomOP classRoomOP)
        {
            if (id != classRoomOP.ClassRoom.ClassRoomId) return NotFound();

            var schoolName = Utils.getSchoolName(User, _context);
            if (ModelState.IsValid)
            {
                classRoomOP.ClassRoom.StudentsPlaces = StudentsPlaceListToString(classRoomOP);
                try
                {
                    var previousList = _context.Absence.Where(a => a.ClassRoomId == id && a.SchoolName == schoolName).ToList();
                    _context.Update(classRoomOP.ClassRoom);
                    await _context.SaveChangesAsync();

                    //Criação das faltas correspondentes
                    List<Absence> actualList = GetAbsencesList(id);
                    //_context.Absence.RemoveRange(list);


                    //Exemplo: Aluno apareceu atrasado
                    foreach (Absence previousA in previousList)
                    {
                        Absence abs = null;
                        foreach (Absence actualA in actualList)
                        {
                            if (previousA.StudentId == actualA.StudentId) abs = actualA;
                        }
                        if (abs == null) _context.Absence.Remove(previousA); //Remove os que já não estão a faltar
                        else //Dá update dos que continuam a faltar
                        {
                            previousA.Justification = abs.Justification;
                            _context.Absence.Update(previousA);
                        }
                    }

                    //Adiciona os que estão a faltar
                    foreach (Absence actualA in actualList)
                    {
                        Absence abs = null;
                        foreach (Absence previousA in previousList)
                        {
                            if (actualA.StudentId == previousA.StudentId) abs = actualA;
                        }

                        if (abs == null) 
                        { 
                            _context.Absence.Add(actualA);

                            var student = _context.Users.FirstOrDefault(u => u.Id == actualA.StudentId && u.SchoolName == schoolName);
                            var classroom = _context.ClassRoom.FirstOrDefault(u => u.ClassRoomId == actualA.ClassRoomId && u.SchoolName == schoolName);
                            var uc = _context.UC.FirstOrDefault(u => u.UCId == classroom.UCId && u.SchoolName == schoolName);

                            if (student.SecondaryEmail != null)
                            {
                                //enviar email para encarregado de educação
                                await _emailSender.SendEmailAsync(student.SecondaryEmail, $"GEscola - Falta de {student.Name}",
                                $"Olá, <br/>O seu educando {student.Name} faltou à disciplina {uc.Name}.<br/> Por favor vá a GEscola justificar a falta.<br/><br/>Atenciosamente,<br/>GEscola");
                            }
                        }
                    }

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassRoomExists(id)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }

            var course = await _context.Course.FirstOrDefaultAsync(c => c.CourseId == classRoomOP.ClassRoom.CourseId && c.SchoolName == schoolName);
            ViewBag.course = course.Name;
            var classe = await _context.SchoolClass.FirstOrDefaultAsync(sc => sc.SchoolClassId == classRoomOP.ClassRoom.SchoolClassId && sc.SchoolName == schoolName);
            ViewBag.classe = classe.Name;
            var teacher = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(classRoomOP.ClassRoom.TeacherId) && u.SchoolName == schoolName);
            ViewBag.teacher = teacher.Name;
            ViewBag.ucs = GetListOfUCs(classRoomOP.ClassRoom.CourseId);
            ViewBag.rooms = GetListOfRooms();
            ViewBag.students = StringToList(classRoomOP.ClassRoom.StudentsPlaces);
            return View(classRoomOP);
        }

        /// <summary>
        /// This method of SchoolClassesController returns true if exists a 
        /// class room with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">int Id of the class room we want to find</param>
        /// <returns>true if the class room exists, otherwise returns false</returns>
        private bool ClassRoomExists(int id)
        {
            return _context.ClassRoom.Any(e => e.ClassRoomId == id);
        }
    }
}
