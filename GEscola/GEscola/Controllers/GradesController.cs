﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using GEscola.Data;
using GEscola.Models;
using System.Data;
using IronPdf;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace GEscola.Controllers
{
    public class GradesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ICompositeViewEngine _viewEngine;

        public GradesController(ApplicationDbContext context, ICompositeViewEngine viewEngine)
        {
            _context = context;
            _viewEngine = viewEngine;
        }

        //ViewBag List of UC's 
        /// <summary>
        /// This method of GradesController returns the list of UCs, of the logged user school,
        /// of the given Evaluation.
        /// </summary>
        /// <param name="evaluation">Evaluation evaluation to find</param>
        /// <returns>List of UC ucsOfAnEvaluation</returns>
        public List<UC> GetListOfUCs(Evaluation evaluation)
        {
            var schoolName = Utils.getSchoolName(User, _context);
            var stringOfUCs = _context.Course.FirstOrDefault(c => c.CourseId == evaluation.CourseId && c.SchoolName == schoolName).UCs;
            List<string> list = new List<string>(stringOfUCs.Split('|'));


            List<UC> listOfUCs = new List<UC>();
            foreach (UC uc in _context.UC.Where(u => u.SchoolName == schoolName).OrderBy(uc => uc.Name))
            {
                if (list.Contains(uc.UCId.ToString())) listOfUCs.Add(uc);
            }
            return listOfUCs;
        }

        // GET: Grade/Create
        /// <summary>
        /// This method of GradesController returns the Create View that
        /// is used to create a grade for the Evaluation that contains the given
        /// courseId and classId, for the logged user school.
        /// </summary>
        /// <param name="courseID">int courseId to find</param>
        /// <param name="classID">int classID to find</param>
        /// <returns>View(GradeOP gradeOP)</returns>
        public IActionResult Create(int courseID, int classID)
        {
            var actualLectiveYear = Utils.getLectiveYear(User, _context);
            var schoolName = Utils.getSchoolName(User, _context);
            var evaluation = _context.Evaluation.FirstOrDefault(e => e.CourseId == courseID && e.SchoolClassId == classID && e.SchoolName == schoolName && e.LectiveYear == actualLectiveYear);
            var courseName = _context.Course.FirstOrDefault(c => c.CourseId == evaluation.CourseId && c.SchoolName == schoolName).Name;
            var className = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == evaluation.SchoolClassId && sc.SchoolName == schoolName && sc.LectiveYear == actualLectiveYear).Name;
            ViewBag.courseName = courseName;
            ViewBag.className = className;
            ViewBag.ucs = GetListOfUCs(evaluation);
            ViewBag.students = Utils.GetStudentsInClass(evaluation.SchoolClassId, User, _context);
            GradeOP gradeOP = new GradeOP { GradesList = GetListOfGradesUsingEvaluation(evaluation) };

            return View(gradeOP);
        }

        // POST: Grades/Create
        /// <summary>
        /// This async method of GradesController redirects to the Index View of the EvaluationsController 
        /// that shows all the evaluations of the logged user school. <br/>
        /// This method is used to create all grades with the given form info.
        /// </summary>
        /// <param name="gradeOP">GradeOP form with the grades info</param>
        /// <returns>Redirects to the Index View of the EvaluationsController</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GradeOP gradeOP)
        {
            foreach(Grade grade in gradeOP.GradesList)
            {
                if(grade.GradeNumber >= 0 && grade.GradeNumber <= 20)
                {
                    grade.SchoolName = Utils.getSchoolName(User, _context);
                    grade.LectiveYear = Utils.getLectiveYear(User, _context);
                    _context.Add(grade);
                }
            }
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Evaluations");            
        }

        /// <summary>
        /// This method of GradesController returns an empty list of grades for an evaluation form.
        /// </summary>
        /// <param name="evaluation">Evaluation evaluation to get all grades</param>
        /// <returns>List of Grade evaluationGradesEmptyList</returns>
        public List<Grade> GetListOfGradesUsingEvaluation(Evaluation evaluation)
        {
            List<Grade> list = new List<Grade>();
            var ucs = GetListOfUCs(evaluation);
            var students = Utils.GetStudentsInClass(evaluation.SchoolClassId, User, _context);

            foreach (Person p in students)
            {
                foreach (UC uc in ucs)
                {
                    list.Add(new Grade { SchoolName = Utils.getSchoolName(User, _context), LectiveYear = Utils.getLectiveYear(User, _context), EvaluationID = evaluation.EvaluationId, StudentID = p.Id, UCID = uc.UCId, GradeNumber = 0 });

                }
            }
            return list;
        }

        /// <summary>
        /// This method of GradesController returns the list of grades for of the given evaluation id.
        /// </summary>
        /// <param name="evaluationID">int evaluationId to find</param>
        /// <returns>List of Grade evaluationGradesList</returns>
        public List<Grade> GetListOfGrades(int evaluationID)
        {
            List<Grade> list = new List<Grade>();
            var grades = _context.Grade.Where(g => g.EvaluationID == evaluationID && g.SchoolName == Utils.getSchoolName(User, _context) && g.LectiveYear == Utils.getLectiveYear(User, _context));
            foreach (Grade g in grades)
            {
                list.Add(g);
            }
            return list;
        }


        /// <summary>
        /// This method of EvaluationsController returns the user grades average with the given student email.
        /// </summary>
        /// <returns>float grades average of the user</returns>
        public float GetStudentAverage(string studentEmail)
        {
            var student = _context.Users.FirstOrDefault(u => u.Email == studentEmail);

            List<int> grades = new List<int>();
            foreach (var g in _context.Grade.Where(g => g.StudentID == student.Id).ToList())
            {
                grades.Add(g.GradeNumber);
            }

            return ((float)grades.Sum() / (float)grades.Count());
        }

        /// <summary>
        /// This method of EvaluationsController returns the Dictionary of PersonKey and floatValue
        /// that contains the grade average of each student based on the evaluations already created,
        /// of the logged user school, of the actual lective year.
        /// </summary>
        /// <returns>Dictionary of PersonKey and floatValue studentsGradesAverage</returns>
        public Dictionary<Person, float> studentsAverage()
        {
            var map = new Dictionary<Person, float>();

            List<int> schoolClassIDsInEvaluations = new List<int>();
            List<SchoolClass> schoolClassesInEvaluations = new List<SchoolClass>();
            foreach (Evaluation e in _context.Evaluation.Where(u => u.SchoolName == Utils.getSchoolName(User, _context) && u.LectiveYear == Utils.getLectiveYear(User, _context)))
            {
                if (!schoolClassIDsInEvaluations.Contains(e.SchoolClassId))
                {
                    schoolClassIDsInEvaluations.Add(e.SchoolClassId);
                    schoolClassesInEvaluations.Add(_context.SchoolClass.FirstOrDefault(s => s.SchoolClassId == e.SchoolClassId && s.LectiveYear == Utils.getLectiveYear(User, _context)));
                }
            }

            List<Person> studentsList = new List<Person>();
            foreach (SchoolClass sc in schoolClassesInEvaluations)
            {
                studentsList.AddRange(Utils.GetStudentsInClass(sc.SchoolClassId, User, _context));
            }

            foreach (Person p in studentsList)
            {
                map.Add(p, GetStudentAverage(p.Email));
            }
            return map;
        }

        /// <summary>
        /// This method of EvaluationsController returns the Dictionary of PersonKey and floatValue
        /// that contains the grade average of the best student based on the evaluations of the
        /// school class with the given school class id, of the logged user school, of the actual lective year.
        /// </summary>
        /// <param name="schoolClassId">int schoolClassId to find students of the school class</param>
        /// <returns>Dictionary of PersonKey and floatValue of the grade average of the best student</returns>
        public Dictionary<Person, float> bestStudentAverageOfSchoolClass(int schoolClassId)
        {
            var findingBest = new Dictionary<Person, float>();
            Person bestStudent = null;
            float bestAverage = -1;
            foreach (Person p in Utils.GetStudentsInClass(schoolClassId, User, _context))
            {
                var avg = GetStudentAverage(p.Email);
                if (avg > bestAverage)
                {
                    bestAverage = avg;
                    bestStudent = p;
                }
            }

            findingBest.Add(bestStudent, bestAverage);
            return findingBest;
        }

        /// <summary>
        /// This method of EvaluationsController returns the Dictionary of PersonKey and floatValue
        /// that contains the grade average of the worst student based on the evaluations of the
        /// school class with the given school class id, of the logged user school, of the actual lective year.
        /// </summary>
        /// <param name="schoolClassId">int schoolClassId to find students of the school class</param>
        /// <returns>Dictionary of PersonKey and floatValue of the grade average of the worst student</returns>
        public Dictionary<Person, float> worstStudentAverageOfSchoolClass(int schoolClassId)
        {
            var findingWorst = new Dictionary<Person, float>();
            Person worstStudent = null;
            float worstAverage = 21;
            foreach (Person p in Utils.GetStudentsInClass(schoolClassId, User, _context))
            {
                var avg = GetStudentAverage(p.Email);
                if (avg < worstAverage)
                {
                    worstAverage = avg;
                    worstStudent = p;
                }
            }

            findingWorst.Add(worstStudent, worstAverage);
            return findingWorst;
        }

        /// <summary>
        /// This method of GradesController returns PreviewPDF View tha shows the grades of
        /// the given school class of the given course.
        /// </summary>
        /// <param name="courseID">int courseID to find</param>
        /// <param name="classID">int classID to find</param>
        /// <returns>View(GradeOP gradeOP)</returns>
        public IActionResult PreviewPDF(int courseID, int classID)
        {
            var actualLectiveYear = Utils.getLectiveYear(User, _context);
            var schoolName = Utils.getSchoolName(User, _context);
            var evaluation = _context.Evaluation.FirstOrDefault(e => e.CourseId == courseID && e.SchoolClassId == classID && e.SchoolName == schoolName && e.LectiveYear == actualLectiveYear);
            ViewBag.courseName = _context.Course.FirstOrDefault(c => c.CourseId == courseID && c.SchoolName == schoolName).Name;
            ViewBag.className = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == classID && sc.SchoolName == schoolName && sc.LectiveYear == actualLectiveYear).Name;
            ViewBag.ucs = GetListOfUCs(evaluation);
            ViewBag.students = Utils.GetStudentsInClass(evaluation.SchoolClassId, User, _context);
            ViewBag.courseID = courseID;
            ViewBag.classID = classID;
            ViewBag.bestStudent = bestStudentAverageOfSchoolClass(classID);
            ViewBag.worstStudent = worstStudentAverageOfSchoolClass(classID);
            GradeOP gradeOP = new GradeOP { GradesList = GetListOfGrades(evaluation.EvaluationId) };

            return View(gradeOP);
        }

        /// <summary>
        /// This async method of GradesController redirects to the PreviewPDF View tha shows the grades of
        /// the given school class of the given course.
        /// This method converts a HTML page into a PDF file.
        /// </summary>
        /// <param name="courseID">int courseID to find</param>
        /// <param name="classID">int classID to find</param>
        /// <returns>Redirects to the PreviewPDF View</returns>
        public async Task<ActionResult> PrintPDFAsync(int courseID, int classID)
        {
            var actualLectiveYear = Utils.getLectiveYear(User, _context);
            var schoolName = Utils.getSchoolName(User, _context);
            var evaluation = _context.Evaluation.FirstOrDefault(e => e.CourseId == courseID && e.SchoolClassId == classID && e.SchoolName == schoolName && e.LectiveYear == actualLectiveYear);
            ViewBag.courseName = _context.Course.FirstOrDefault(c => c.CourseId == courseID && c.SchoolName == schoolName).Name;
            ViewBag.className = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == classID && sc.SchoolName == schoolName && sc.LectiveYear == actualLectiveYear).Name;
            ViewBag.ucs = GetListOfUCs(evaluation);
            ViewBag.students = Utils.GetStudentsInClass(evaluation.SchoolClassId, User, _context);
            ViewBag.bestStudent = bestStudentAverageOfSchoolClass(classID);
            ViewBag.worstStudent = worstStudentAverageOfSchoolClass(classID);
            GradeOP gradeOP = new GradeOP { GradesList = GetListOfGrades(evaluation.EvaluationId) };

            var htmlToPdf = new HtmlToPdf();

            //var pdfDocument = htmlToPdf.RenderHtmlAsPdf("<h1>Se estás a ver isto só queria dizer que te amo muito e que não te quero ver assim <3</h1>");
            var viewRendered = await RenderRazorViewToStringAsync("PrintPDF", gradeOP);

            var pdfDocument = htmlToPdf.RenderHtmlAsPdf(viewRendered);//WTFFFFFFFFFFFFF
            pdfDocument.SaveAs("Docs/GEscola - Notas " + ViewBag.className + " (" + ViewBag.courseName + ").pdf");
            
            View(gradeOP);
            return RedirectToAction(nameof(PreviewPDF), new { courseID = courseID, classID = classID});
        }

        /// <summary>
        /// This async method of GradesController returns the converted HTML page into a string.
        /// </summary>
        /// <param name="viewName">string viewName to convert</param>
        /// <param name="model">object model to use in the view to convert</param>
        /// <returns>string of the HTML page</returns>
        public async Task<string> RenderRazorViewToStringAsync(string viewName, object model)
        {
            ViewData.Model = model;
            using (var writer = new StringWriter())
            {
                ViewEngineResult viewResult =
                    _viewEngine.FindView(ControllerContext, viewName, false);

                ViewContext viewContext = new ViewContext(
                    ControllerContext,
                    viewResult.View,
                    ViewData,
                    TempData,
                    writer,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);

                return writer.GetStringBuilder().ToString();
            }
        }
    }
}
