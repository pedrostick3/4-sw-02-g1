﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace GEscola.Controllers
{
    public class AbsencesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;

        public AbsencesController(ApplicationDbContext context, IEmailSender emailSender)
        {
            _context = context;
            _emailSender = emailSender;
        }

        /// <summary>
        /// This method of AbsencesController returns the list of all absences for the
        /// logged user that can be in the "Student", "Teacher" or "ClassDirector" roles.
        /// </summary>
        /// <returns>List of Absence absencesList</returns>
        public List<Absence> getAbsencesForStudentTeacherOrDT()
        {
            var userID = Utils.getLoggedUser(User, _context).Id;
            var schoolName = Utils.getSchoolName(User, _context);

            List<int> classRoomIDs = new List<int>();
            foreach (ClassRoom cr in _context.ClassRoom.Where(c => c.TeacherId == userID && c.SchoolName == schoolName))
            {
                classRoomIDs.Add(cr.ClassRoomId);
            }

            var schoolClass = _context.SchoolClass.FirstOrDefault(sc => sc.TeacherId == userID && sc.SchoolName == schoolName);
            List<int> classRoomDTsIDs = new List<int>();
            if (schoolClass != null)
            {
                foreach (ClassRoom c in _context.ClassRoom.Where(c => c.SchoolClassId == schoolClass.SchoolClassId && c.SchoolName == schoolName))
                {
                    classRoomDTsIDs.Add(c.ClassRoomId);
                }

            }

            var absences = _context.Absence.Where(a => (a.StudentId == userID || classRoomIDs.Contains(a.ClassRoomId) || classRoomDTsIDs.Contains(a.ClassRoomId)) && a.SchoolName == schoolName).ToList();
            return absences;
        }

        // GET: Absences
        /// <summary>
        /// This method of AbsencesController returns the Index View
        /// that shows all the absences for the logged user.
        /// </summary>
        /// <returns>View(List of Absence absencesToShow)</returns>
        public IActionResult Index()
        {
            var schoolName = Utils.getSchoolName(User, _context);

            List<AbsenceOP> absenceOPs = new List<AbsenceOP>();
            foreach (Absence a in getAbsencesForStudentTeacherOrDT())
            {
                var cr = _context.ClassRoom.FirstOrDefault(cr => cr.isDeleted == false && cr.ClassRoomId == a.ClassRoomId && cr.SchoolName == schoolName);
                absenceOPs.Add(new AbsenceOP
                {
                    Absence = a,
                    Course = _context.Course.FirstOrDefault(c => c.CourseId == cr.CourseId && c.SchoolName == schoolName).Name,
                    SchoolClass = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == cr.SchoolClassId && sc.SchoolName == schoolName).Name,
                    Teacher = _context.Users.FirstOrDefault(u => u.Id.Equals(cr.TeacherId) && u.SchoolName == schoolName).Name,
                    UC = _context.UC.FirstOrDefault(u => u.UCId == cr.UCId && u.SchoolName == schoolName).Name,
                    Room = _context.Room.FirstOrDefault(r => r.RoomId == cr.RoomId && r.SchoolName == schoolName).Name,
                    Student = _context.Users.FirstOrDefault(u => u.Id.Equals(a.StudentId) && u.SchoolName == schoolName).Name,
                    Date = cr.Date

                });
            }

            return View(absenceOPs);
        }

        // GET: Absences/AcceptedAbsences
        /// <summary>
        /// This method of AbsencesController returns the AcceptedAbsences View
        /// that shows all accepted absences for the logged user.
        /// </summary>
        /// <returns>View(List of Absence acceptedAbsencesToShow)</returns>
        public IActionResult AcceptedAbsences()
        {
            var schoolName = Utils.getSchoolName(User, _context);

            List<AbsenceOP> absenceOPs = new List<AbsenceOP>();
            foreach (Absence a in getAbsencesForStudentTeacherOrDT())
            {
                var cr = _context.ClassRoom.FirstOrDefault(cr => cr.isDeleted == false && cr.ClassRoomId == a.ClassRoomId && a.SchoolName == schoolName);
                absenceOPs.Add(new AbsenceOP
                {
                    Absence = a,
                    Course = _context.Course.FirstOrDefault(c => c.CourseId == cr.CourseId && c.SchoolName == schoolName).Name,
                    SchoolClass = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == cr.SchoolClassId && sc.SchoolName == schoolName).Name,
                    Teacher = _context.Users.FirstOrDefault(u => u.Id.Equals(cr.TeacherId) && u.SchoolName == schoolName).Name,
                    UC = _context.UC.FirstOrDefault(u => u.UCId == cr.UCId && u.SchoolName == schoolName).Name,
                    Room = _context.Room.FirstOrDefault(r => r.RoomId == cr.RoomId && r.SchoolName == schoolName).Name,
                    Student = _context.Users.FirstOrDefault(u => u.Id.Equals(a.StudentId) && u.SchoolName == schoolName).Name,
                    Date = cr.Date

                });
            }

            return View(absenceOPs);
        }

        // GET: Absences/Details/5
        /// <summary>
        /// This async method of AbsencesController returns the Details View
        /// that shows with details the absence with the given id.
        /// </summary>
        /// <param name="id">int id of the absence</param>
        /// <returns>View(Absence absenceToShow)</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();
            var absence = await _context.Absence.FirstOrDefaultAsync(m => m.AbsenceId == id);
            if (absence == null) return NotFound();

            var schoolName = Utils.getSchoolName(User, _context);
            var cr = _context.ClassRoom.FirstOrDefault(c => c.isDeleted == false && c.ClassRoomId == absence.ClassRoomId && c.SchoolName == schoolName);

            var absOP = new AbsenceOP
            {
                Absence = absence,
                Course = _context.Course.FirstOrDefault(c => c.CourseId == cr.CourseId && c.SchoolName == schoolName).Name,
                SchoolClass = _context.SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == cr.SchoolClassId && sc.SchoolName == schoolName).Name,
                Teacher = _context.Users.FirstOrDefault(u => u.Id.Equals(cr.TeacherId) && u.SchoolName == schoolName).Name,
                UC = _context.UC.FirstOrDefault(u => u.UCId == cr.UCId && u.SchoolName == schoolName).Name,
                Room = _context.Room.FirstOrDefault(r => r.RoomId == cr.RoomId && r.SchoolName == schoolName).Name,
                Student = _context.Users.FirstOrDefault(u => u.Id.Equals(absence.StudentId) && u.SchoolName == schoolName).Name,
                Date = cr.Date

            };

            return View(absOP);
        }

        // GET: Evaluations/ParentalPIN
        /// <summary>
        /// This async method of AbsencesController returns the ParentalPIN View
        /// that shows with security form to insert the parental PIN to justify 
        /// an absence of a given id.
        /// </summary>
        /// <param name="id">int id of the absence</param>
        /// <returns>View(Absence absenceToJustify)</returns>
        public async Task<IActionResult> ParentalPIN(int? id)
        {
            if (id == null) return NotFound();
            var abs = await _context.Absence.FindAsync(id);
            if (abs == null) return NotFound();

            var student = _context.Users.FirstOrDefault(u => u.Id == abs.StudentId);

            if(BirthAgeHigherThan18(student.Birthdate)) return RedirectToAction(nameof(Justify), new { id = id });

            abs.ClassRoomId = 0;
            return View(abs);
        }

        // POST: Evaluations/ParentalPIN
        /// <summary>
        /// This async method of AbsencesController redirects to the Justify View 
        /// that justifies an absence with the given id, if the parental pin was
        /// correct, otherwise returns the same View with the respective errors. <br/>
        /// This method is used to verify if the parental PIN is correct.
        /// </summary>
        /// <param name="id">int id of absence</param>
        /// <param name="absence">Absence form with the absence info</param>
        /// <returns>Redirects to the Justify View if the parental PIN is correct,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ParentalPIN(int id, Absence absence)
        {
            if (id != absence.AbsenceId) return NotFound();

            if (absence.ClassRoomId != _context.Users.FirstOrDefault(u => u.Id == absence.StudentId && u.SchoolName == Utils.getSchoolName(User, _context)).ParentalPIN)
            {
                ModelState.AddModelError(string.Empty, "PIN Parental incorreto.");
                return View(absence);
            }
            return RedirectToAction(nameof(Justify), new { id = id });
        }

        ///<summary>
        /// This method of AbsencesController returns true if the student with
        /// given birthdate is higher than 18 years old, otherwise returns false.
        /// </summary>
        /// <param name="bday">DateTime birthDate of student</param>
        /// <returns>true if the student is higher than 18 years old, otherwise returns false</returns>
        public bool BirthAgeHigherThan18(DateTime bday)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age)) age--;
            if (age < 18) return false;
            return true;
        }

        // GET: Absences/Justify/5
        /// <summary>
        /// This async method of AbsencesController returns the Justify View
        /// that is used to justify the absence with the given id.
        /// </summary>
        /// <param name="id">int id of absence</param>
        /// <returns>View(Absence absenceToJustify)</returns>
        public async Task<IActionResult> Justify(int? id)
        {
            if (id == null) return NotFound();
            var absence = await _context.Absence.FindAsync(id);
            if (absence == null) return NotFound();

            return View(absence);
        }

        // POST: Absences/Justify/5
        /// <summary>
        /// This async method of AbsencesController redirects to the Index View 
        /// that shows all absences for the logged user, if the model is valid,
        /// otherwise returns the same View with the respective errors. <br/>
        /// This method is used to justify the student absence.
        /// </summary>
        /// <param name="id">int id of absence</param>
        /// <param name="absence">Absence form with the absence info</param>
        /// <returns>Redirects to the Index View if the model is valid,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Justify(int id, Absence absence)
        {
            if (id != absence.AbsenceId) return NotFound();
            if(absence.Justification == null || absence.Justification == "")
            {
                ModelState.AddModelError(string.Empty, "A justificação não pode estar vazia.");
            }
            else if (ModelState.IsValid)
            {
                try
                {
                    //absence.isDeleted = true;
                    _context.Update(absence);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AbsenceExists(absence.AbsenceId)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(absence);
        }


        // GET: Absences/Accept/5
        /// <summary>
        /// This async method of AbsencesController returns the Accept View
        /// that is used by users of role "ClassDirector" to accept a justification
        /// of the absence with the given id.
        /// </summary>
        /// <param name="id">int id of absence</param>
        /// <returns>View(Absence absenceToAccept)</returns>
        public async Task<IActionResult> Accept(int? id)
        {
            if (id == null) return NotFound();
            var absence = await _context.Absence.FirstOrDefaultAsync(m => m.AbsenceId == id);
            if (absence == null) return NotFound();

            absence.isDeleted = true;
            _context.Absence.Update(absence);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        // GET: Absences/Decline/5
        /// <summary>
        /// This async method of AbsencesController returns the Decline View
        /// that is used by users of role "ClassDirector" to decline a justification
        /// of the absence with the given id.
        /// </summary>
        /// <param name="id">int id of absence</param>
        /// <returns>View(Absence absenceToDecline)</returns>
        public async Task<IActionResult> Decline(int? id)
        {
            if (id == null) return NotFound();
            var absence = await _context.Absence.FirstOrDefaultAsync(m => m.AbsenceId == id);
            if (absence == null) return NotFound();

            var schoolName = Utils.getSchoolName(User, _context);
            var student = _context.Users.FirstOrDefault(u => u.Id.Equals(absence.StudentId) && u.SchoolName == schoolName);
            var classRoom = _context.ClassRoom.FirstOrDefault(c => c.ClassRoomId == absence.ClassRoomId && c.SchoolName == schoolName);
            var uc = _context.UC.FirstOrDefault(uc => uc.UCId == classRoom.UCId && uc.SchoolName == schoolName);
            if (student.SecondaryEmail != null)
            {
                //enviar email para encarregado de educação
                await _emailSender.SendEmailAsync(student.SecondaryEmail, "GEscola - Justificação inválida",
                $"Olá, <br/>A justificação da falta do seu educando {student.Name} à disciplina {uc.Name} não foi aceite pelo Diretor de Turma, deste modo, pedimos que faça uma nova justificação para que a justificação da falta possa ser validada. <br/><br/> Justificação recusada:<br/> \"{absence.Justification}\" <br/><br/>Atenciosamente,<br/>GEscola");
            }

            absence.Justification = null;
            _context.Absence.Update(absence);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// This method of AbsencesController returns true if exists an absence
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">int id of absence</param>
        /// <returns>true if the absence exists, otherwise returns false</returns>
        private bool AbsenceExists(int id)
        {
            return _context.Absence.Any(e => e.AbsenceId == id);
        }
    }
}
