﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Identity;

namespace GEscola.Controllers
{
    public class ProfileController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Person> _userManager; 
        private readonly SignInManager<Person> _signInManager;

        public ProfileController(ApplicationDbContext context, UserManager<Person> userManager, SignInManager<Person> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /// <summary>
        /// This method of ProfileController returns the user logged school grades average.
        /// </summary>
        /// <returns>float grades average of the school of the user logged</returns>
        public float GetSchoolAverage()
        {
            List<Person> students = _context.Users.Where(u => u.SchoolName == Utils.getSchoolName(User, _context)).ToList();
            List<Person> secondary = new List<Person>(students);
            foreach(Person p in secondary)
            {
                if (Utils.getUserRole(p.Email, _context) != "Student") students.Remove(p);
            }
            List<string> studentsID = new List<string>();
            foreach (Person p in students) studentsID.Add(p.Id);

            List<int> grades = new List<int>(); 
            foreach(var g in _context.Grade.Where(g => g.LectiveYear == Utils.getLectiveYear(User, _context)).ToList())
            {
                if (studentsID.Contains(g.StudentID)) grades.Add(g.GradeNumber);
            }

            return ((float)grades.Sum()/(float)grades.Count());
        }

        /// <summary>
        /// This method of ProfileController returns the user logged grades average.
        /// </summary>
        /// <returns>float grades average of the user logged</returns>
        public float GetSelfAverage()
        {
            var userLogged = Utils.getLoggedUser(User, _context);

            List<int> grades = new List<int>();
            foreach (var g in _context.Grade.Where(g => g.StudentID == userLogged.Id).ToList())
            {
                grades.Add(g.GradeNumber);
            }

            return ((float)grades.Sum() / (float)grades.Count());
        }

        // GET: Profile?email=
        /// <summary>
        /// This method of ProfileController returns the Profile View
        /// that shows the profile of the user with the given email.
        /// </summary>
        /// <param name="email">string email of the user</param>
        /// <returns>View(Person userToShow)</returns>
        public IActionResult Profile(string email)
        {
            //verificar se user é admin
            if(User.IsInRole("Admin")) ViewBag.averageSchoolGrades = GetSchoolAverage();

            //verificar se user é student
            if (User.IsInRole("Student")) ViewBag.averageSelfGrades = GetSelfAverage();

            email = email.Replace(" ", "@").Replace("^", ".");
            var person = _context.Users.FirstOrDefault(user => user.Email.Equals(email));
            var user = new Users { person = person, role = Utils.getUserRole(email, _context) };
            return View(user);
        }

        // Get View using this Method
        /// <summary>
        /// This method of ProfileController returns the ChangePassword View
        /// that shows all the change password form.
        /// </summary>
        /// <returns>View()</returns>
        public ActionResult ChangePassword()
        {
            return View();
        }

        //Save Details of New Password using Identity
        /// <summary>
        /// This async method of ProfileController redirects redirects to the Profile View 
        /// that shows profile of the user if the password was changed successfully, 
        /// otherwise returns the same View with the respective errors. <br/>
        /// This method is used to change de password of an user using the given model.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel form with passwords data</param>
        /// <returns>Redirects to the Profile View if the password was changed successfully,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var user = Utils.getLoggedUser(User, _context);
            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                if (user != null) await _signInManager.SignInAsync(user, isPersistent: false);
                TempData["Success"] = "Password alterada com sucesso!";
                return RedirectToAction("Profile", new { email = user.Email.Replace("@", " ").Replace(".", "^") });
            }
            ModelState.AddModelError("InvalidAttempt", "Dados inválidos. Tente novamente.");
            return View(model);
        }
    }
}
