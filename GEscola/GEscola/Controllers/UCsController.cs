﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;

namespace GEscola.Controllers
{
    public class UCsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UCsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: UCs
        /// <summary>
        /// This async method of UCsController returns the Index View
        /// that shows all the active UCs of the logged user school.
        /// </summary>
        /// <returns>View(List of UC ucsToShow)</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.UC.Where(u => u.isDeleted == false && u.SchoolName == Utils.getSchoolName(User, _context)).ToListAsync());
        }

        // GET: UCs/Details/5
        /// <summary>
        /// This async method of UCsController returns the Details View
        /// that shows with details the UC with the given id.
        /// </summary>
        /// <param name="id">int id of the UC</param>
        /// <returns>View(UC ucToShow)</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();
            var uC = await _context.UC.FirstOrDefaultAsync(m => m.UCId == id);
            if (uC == null) return NotFound();

            return View(uC);
        }

        /// <summary>
        /// This method of UCsController returns the list of users with
        /// roles Teacher and ClassDirector of the logged user school.
        /// </summary>
        /// <returns>List of SelectListItem usersOfRoleTeacherAndClassDirector</returns>
        public List<SelectListItem> GetListOfTeachersAndDTs()
        {
            List<SelectListItem> listTeachers = new List<SelectListItem>();
            foreach (Person p in Utils.TeachersAndDTsUsers(User, _context))
            {
                listTeachers.Add(new SelectListItem { Text = p.Name + " - " + p.Email, Value = p.Id });
            }
            return listTeachers;
        }

        // GET: UCs/Create
        /// <summary>
        /// This method of UCsController returns the Create View that
        /// is used to create a UC for the logged user school.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult Create()
        {
            ViewBag.teachers = GetListOfTeachersAndDTs();    
            return View();
        }

        // POST: UCs/Create
        /// <summary>
        /// This async method of UCsController redirects to the Index View 
        /// that shows all the active UCs for the logged user school if the UC
        /// was well created, otherwise returns the same View with the respective errors. <br/>
        /// This method is used to create a UC with the given form info.
        /// </summary>
        /// <param name="uc">UC form with the UC info</param>
        /// <returns>Redirects to the Index View if the UC was well created,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UC uc)
        {
            if (uc.TeacherId == "null")
            {
                ModelState.AddModelError(string.Empty, "É obrigatório a seleção do Professor Responsável.");
            }
            else if (ModelState.IsValid)
            {
                var ucExists = _context.UC.FirstOrDefault(u => u.Name.Equals(uc.Name));
                if (ucExists != null)
                {
                    ModelState.AddModelError(string.Empty, "O nome inserido para a nova UC já está um uso. Insira outro diferente.");
                }
                else
                {
                    var uCurricular = new UC { Name = uc.Name, TeacherId = uc.TeacherId, SchoolName = Utils.getSchoolName(User, _context), Description = uc.Description, TeacherName = GetResponsibleTeacherName(uc.TeacherId) };
                    _context.Add(uCurricular);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }

            ViewBag.teachers = GetListOfTeachersAndDTs();
            return View(uc);
        }

        /// <summary>
        /// This method of UCsController returns the responsible teacher name
        /// that is the user of the given user id.
        /// </summary>
        /// <param name="id">string id of user</param>
        /// <returns>string userName</returns>
        public string GetResponsibleTeacherName(string id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id && u.isDeleted == false);
            return user.Name;
        }


        // GET: UCs/Edit/5
        /// <summary>
        /// This async method of UCsController returns the Edit View
        /// that is used to edit the UC of the given id.
        /// </summary>
        /// <param name="id">string id of the UC to edit</param>
        /// <returns>View(UC ucToEdit)</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            var uC = await _context.UC.FindAsync(id);
            if (uC == null) return NotFound();

            ViewBag.teachers = GetListOfTeachersAndDTs();
            return View(uC);
        }

        // POST: UCs/Edit/5
        /// <summary>
        /// This async method of UCsController redirects to the Index View that shows 
        /// all the active UCs of the logged user school if the UC was well edited,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to edit a UC of the given id with the given changes.
        /// </summary>
        /// <param name="id">string id of the UC to edit</param>
        /// <param name="uC">UC form with the ucChanges</param>
        /// <returns>View depending if the UC was well edited or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UC uC)
        {
            if (id != uC.UCId) return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    uC.TeacherName = GetResponsibleTeacherName(uC.TeacherId);
                    _context.Update(uC);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UCExists(uC.UCId)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }

            ViewBag.teachers = GetListOfTeachersAndDTs();
            return View(uC);
        }

        // GET: UCs/Delete/5
        /// <summary>
        /// This async method of UCsController returns the Delete View
        /// that is used to delete the UC of the given id.
        /// </summary>
        /// <param name="id">string id of the UC to delete</param>
        /// <returns>View(UC ucToDelete)</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            var uC = await _context.UC.FirstOrDefaultAsync(m => m.UCId == id);
            if (uC == null) return NotFound();

            return View(uC);
        }

        // POST: UCs/Delete/5
        /// <summary>
        /// This async method of UCsController redirects to the Index View that shows 
        /// all the active UCs of the logged user school. <br/>
        /// This method is used to delete the UC of the given id.
        /// </summary>
        /// <param name="id">string id of the UC to delete</param>
        /// <returns>Redirects to the Index View</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var uC = await _context.UC.FindAsync(id);
            uC.isDeleted = true;
            _context.UC.Update(uC);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// This method of UCsController returns true if exists a UC
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">string Id of the UC we want to find</param>
        /// <returns>true if the UC exists, otherwise returns false</returns>
        private bool UCExists(int id)
        {
            return _context.UC.Any(e => e.UCId == id);
        }
    }
}
