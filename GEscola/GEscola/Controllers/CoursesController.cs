﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;

namespace GEscola.Controllers
{
    public class CoursesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CoursesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Courses
        /// <summary>
        /// This async method of CoursesController returns the Index View
        /// that shows all the active courses of the logged user school.
        /// </summary>
        /// <returns>View(List of Course coursesToShow)</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.Course.Where(c => c.isDeleted == false && c.SchoolName == Utils.getSchoolName(User, _context)).ToListAsync());
        }

        // GET: Courses/Details/5
        /// <summary>
        /// This async method of CoursesController returns the Details View
        /// that shows with details the course with the given id.
        /// </summary>
        /// <param name="id">int id of the course</param>
        /// <returns>View(Course courseToShow)</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();
            var course = await _context.Course.FirstOrDefaultAsync(m => m.CourseId == id);
            if (course == null) return NotFound();

            var model = new CourseOP();
            model.Course = course;
            model.ListCourseUCs = StringToList(course.UCs);

            return View(model);
        }

        /// <summary>
        /// This method of CoursesController returns a List of SelectListItems that
        /// contains all course types.
        /// </summary>
        /// <returns>List of SelectListItem courseTypes</returns>
        public List<SelectListItem> GetListOfCoursesTypes()
        {
            List<SelectListItem> listCourseType = new List<SelectListItem>();
            listCourseType.Add(new SelectListItem { Text = "Curso " + CourseType.Profissional, Value = CourseType.Profissional.ToString() });
            listCourseType.Add(new SelectListItem { Text = "Curso " + CourseType.Regular, Value = CourseType.Regular.ToString() });
            return listCourseType;
        }

        /// <summary>
        /// This method of CoursesController returns a List of CheckBoxs that contains
        /// all UCs of the logged user school.
        /// </summary>
        /// <returns>List of CheckBox ucsList</returns>
        public List<Checkbox> GetListOfUCs() 
        {
            List<Checkbox> listUCs = new List<Checkbox>();
            foreach (UC uc in _context.UC.Where(c => c.isDeleted == false && c.SchoolName == Utils.getSchoolName(User, _context)).ToList())
            {
                listUCs.Add(new Checkbox { IsChecked = false, Name = uc.Name, Value = uc.UCId.ToString() });
            }
            return listUCs;
        }

        // GET: Courses/Create
        /// <summary>
        /// This method of CoursesController returns the Create View that
        /// is used to create a course for the logged user school.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult Create()
        {
            ViewBag.courseTypes = GetListOfCoursesTypes();

            var model = new CourseOP();
            model.ListCourseUCs = GetListOfUCs();
            ViewBag.UCs = _context.UC.Where(u => u.isDeleted == false && u.SchoolName == Utils.getSchoolName(User, _context));
            return View(model);
        }

        // POST: Courses/Create
        /// <summary>
        /// This async method of CoursesController redirects to the Index View 
        /// that shows all the active courses for the logged user school if the course
        /// was well created, otherwise returns the same View with the respective errors. <br/>
        /// This method is used to create a course with the given form info.
        /// </summary>
        /// <param name="courseOP">CourseOP form with the course info</param>
        /// <returns>Redirects to the Index View if the course was well created,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CourseOP courseOP)
        {
            if (ModelState.IsValid)
            {
                var courseExists = _context.Course.FirstOrDefault(c => c.Name.Equals(courseOP.Course.Name) && c.SchoolName == Utils.getSchoolName(User, _context));
                if (courseExists != null)
                {
                    ModelState.AddModelError(string.Empty, "O nome inserido para o novo Curso já está um uso. Insira outro diferente.");
                }
                else
                {
                    var vCourse = new Course { SchoolName = Utils.getSchoolName(User, _context), Name = courseOP.Course.Name, Description = courseOP.Course.Description, CourseType = courseOP.Course.CourseType, CourseDuration = courseOP.Course.CourseDuration, UCs = ListToString(courseOP.ListCourseUCs) };
                    _context.Add(vCourse);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }

            ViewBag.courseTypes = GetListOfCoursesTypes();
            return View(courseOP);
        }

        /// <summary>
        /// This method of CoursesController converts a given UCs checkbox list to a string,
        /// returning it.
        /// </summary>
        /// <param name="checkboxList">List of Checkbox ucsCheckBoxList</param>
        /// <returns>string of the UCs</returns>
        public string ListToString(List<Checkbox> checkboxList)
        {
            string list = "";
            if (checkboxList == null) return list;

            foreach (var checkbox in checkboxList)
            {
                if(checkbox.IsChecked) list += checkbox.Value + "|";
            }
            return list;
        }

        /// <summary>
        /// This method of CoursesController converts a given UCs string to a List of Checkboxs,
        /// returning it.
        /// </summary>
        /// <param name="checkboxList">string ucsCheckBoxList</param>
        /// <returns>List of Checkbox ucsCheckBoxList</returns>
        public List<Checkbox> StringToList(string checkboxList)
        {
            if (checkboxList == null) checkboxList = "";
            List<string> list = new List<string>(checkboxList.Split('|'));
            List<Checkbox> listUCs = new List<Checkbox>();
            foreach (var uc in _context.UC.Where(c => c.isDeleted == false && c.SchoolName == Utils.getSchoolName(User, _context)).ToList())
            {
                listUCs.Add(new Checkbox { IsChecked = (list.Contains(uc.UCId.ToString()))?true:false, Name = uc.Name, Value = uc.UCId.ToString() });
            }
            return listUCs;
        }

        // GET: Courses/Edit/5
        /// <summary>
        /// This async method of CoursesController returns the Edit View
        /// that is used to edit the course of the given id.
        /// </summary>
        /// <param name="id">int id of the course to edit</param>
        /// <returns>View(Course courseToEdit)</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            var course = await _context.Course.FindAsync(id);
            if (course == null) return NotFound();

            ViewBag.courseTypes = GetListOfCoursesTypes();
            var model = new CourseOP();
            model.Course = course;
            model.ListCourseUCs = StringToList(course.UCs);
            return View(model);
        }

        // POST: Courses/Edit/5
        /// <summary>
        /// This async method of CoursesController redirects to the Index View that shows 
        /// all the active courses of the logged user school if the course was well edited,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to edit a course of the given id with the given changes.
        /// </summary>
        /// <param name="id">string id of the course to edit</param>
        /// <param name="courseOP">CourseOP form with the courseChanges</param>
        /// <returns>View depending if the course was well edited or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CourseOP courseOP)
        {
            if (id != courseOP.Course.CourseId)return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var course = courseOP.Course;
                    course.UCs = ListToString(courseOP.ListCourseUCs);
                    _context.Update(course);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CourseExists(courseOP.Course.CourseId)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }

            ViewBag.courseTypes = GetListOfCoursesTypes();
            return View(courseOP);
        }

        // GET: Courses/Delete/5
        /// <summary>
        /// This async method of CoursesController returns the Delete View
        /// that is used to delete the course of the given id.
        /// </summary>
        /// <param name="id">string id of the course to delete</param>
        /// <returns>View(Course courseToDelete)</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            var course = await _context.Course.FirstOrDefaultAsync(m => m.CourseId == id);
            if (course == null) return NotFound();

            return View(course);
        }

        // POST: Courses/Delete/5
        /// <summary>
        /// This async method of CoursesController redirects to the Index View that shows 
        /// all the active courses of the logged user school. <br/>
        /// This method is used to delete the course of the given id.
        /// </summary>
        /// <param name="id">string id of the course to delete</param>
        /// <returns>Redirects to the Index View</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var course = await _context.Course.FindAsync(id);
            course.isDeleted = true;
            _context.Course.Update(course);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// This method of CoursesController returns true if exists a course
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">string Id of the course we want to find</param>
        /// <returns>true if the course exists, otherwise returns false</returns>
        private bool CourseExists(int id)
        {
            return _context.Course.Any(e => e.CourseId == id && e.isDeleted == false);
        }
    }
}
