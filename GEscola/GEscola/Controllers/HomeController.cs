﻿using GEscola.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace GEscola.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// This method of HomeController returns the home page.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This method of HomeController returns the error page.
        /// </summary>
        /// <returns>View(ErrorViewModel errorPage)</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
