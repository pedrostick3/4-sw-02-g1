﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;

namespace GEscola.Controllers
{
    public class RoomsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RoomsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Rooms
        /// <summary>
        /// This async method of RoomsController returns the Index View
        /// that shows all the active rooms of the logged user school.
        /// </summary>
        /// <returns>View(List of Room roomsToShow)</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.Room.Where(r => r.isDeleted == false && r.SchoolName == Utils.getSchoolName(User, _context)).ToListAsync());
        }

        // GET: Rooms/Details/5
        /// <summary>
        /// This async method of RoomsController returns the Details View
        /// that shows with details the room with the given id.
        /// </summary>
        /// <param name="id">int id of the room</param>
        /// <returns>View(Room roomToShow)</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();
            var room = await _context.Room.FirstOrDefaultAsync(m => m.RoomId == id);
            if (room == null) return NotFound();

            return View(room);
        }

        // GET: Rooms/Create
        /// <summary>
        /// This method of RoomsController returns the Create View that
        /// is used to create a room for the logged user school.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rooms/Create
        /// <summary>
        /// This async method of RoomsController redirects to the Index View 
        /// that shows all the active rooms for the logged user school if the room
        /// was well created, otherwise returns the same View with the respective errors. <br/>
        /// This method is used to create a room with the given form info.
        /// </summary>
        /// <param name="room">Room form with the room info</param>
        /// <returns>Redirects to the Index View if the room was well created,
        /// otherwise returns the same View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Room room)
        {
            if (ModelState.IsValid)
            {
                room.SchoolName = Utils.getSchoolName(User, _context);
                _context.Add(room);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        // GET: Rooms/Edit/5
        /// <summary>
        /// This async method of RoomsController returns the Edit View
        /// that is used to edit the room of the given id.
        /// </summary>
        /// <param name="id">int id of the room to edit</param>
        /// <returns>View(Room roomToEdit)</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            var room = await _context.Room.FindAsync(id);
            if (room == null) return NotFound();

            return View(room);
        }

        // POST: Rooms/Edit/5
        /// <summary>
        /// This async method of RoomsController redirects to the Index View that shows 
        /// all the active rooms of the logged user school if the room was well edited,
        /// otherwise it returns to the same View with the respective error. <br/>
        /// This method is used to edit a room of the given id with the given changes.
        /// </summary>
        /// <param name="id">int id of the room to edit</param>
        /// <param name="room">Room form with the roomChanges</param>
        /// <returns>View depending if the room was well edited or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Room room)
        {
            if (id != room.RoomId) return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(room);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoomExists(room.RoomId)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        // GET: Rooms/Delete/5
        /// <summary>
        /// This async method of RoomsController returns the Delete View
        /// that is used to delete the room of the given id.
        /// </summary>
        /// <param name="id">string id of the room to delete</param>
        /// <returns>View(Room roomToDelete)</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            var room = await _context.Room.FirstOrDefaultAsync(m => m.RoomId == id);
            if (room == null) return NotFound();

            return View(room);
        }

        // POST: Rooms/Delete/5
        /// <summary>
        /// This async method of RoomsController redirects to the Index View that shows 
        /// all the active rooms of the logged user school. <br/>
        /// This method is used to delete the room of the given id.
        /// </summary>
        /// <param name="id">string id of the room to delete</param>
        /// <returns>Redirects to the Index View</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var room = await _context.Room.FindAsync(id);
            room.isDeleted = true;
            _context.Update(room);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// This method of RoomsController returns true if exists a room
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">int id of room</param>
        /// <returns>true if the room exists, otherwise returns false</returns>
        private bool RoomExists(int id)
        {
            return _context.Room.Any(e => e.RoomId == id);
        }
    }
}
