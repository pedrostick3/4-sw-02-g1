﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GEscola.Data;
using GEscola.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GEscola.Controllers
{
    public class EvaluationsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EvaluationsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Evaluations
        /// <summary>
        /// This async method of EvaluationsController returns the Index View
        /// that shows all the evaluations of the logged user school, of the actual lective year.
        /// </summary>
        /// <returns>View(List of Evaluation evaluationsToShow)</returns>
        public async Task<IActionResult> Index()
        {
            var actualLectiveYear = Utils.getLectiveYear(User, _context);
            var schoolName = Utils.getSchoolName(User, _context);
            ViewBag.courses = _context.Course.Where(c => c.isDeleted == false && c.SchoolName == schoolName).ToList();
            ViewBag.classes = _context.SchoolClass.Where(c => c.isDeleted == false && c.SchoolName == schoolName && c.LectiveYear == actualLectiveYear).ToList();
            ViewBag.dts = Utils.UsersOfRole("ClassDirector", User, _context);
            ViewBag.CoursesAverage = await CoursesAverageAsync();
            ViewBag.userID = Utils.getLoggedUser(User, _context).Id;
            return View(await _context.Evaluation.Where(e => e.SchoolName == schoolName && e.LectiveYear == actualLectiveYear).ToListAsync());
        }

        // GET: Evaluations/Historic
        /// <summary>
        /// This async method of EvaluationsController returns the Historic View
        /// that shows all the evaluations of the logged user school, of the previous lective years.
        /// </summary>
        /// <returns>View(List of Evaluation evaluationsToShow)</returns>
        public async Task<IActionResult> Historic()
        {
            var actualLectiveYear = Utils.getLectiveYear(User, _context);
            var schoolName = Utils.getSchoolName(User, _context);
            ViewBag.courses = _context.Course.Where(c => c.SchoolName == schoolName).ToList();
            ViewBag.classes = _context.SchoolClass.Where(c => c.isDeleted == false && c.SchoolName == schoolName && c.LectiveYear != actualLectiveYear).ToList();
            ViewBag.dts = Utils.UsersOfRole("ClassDirector", User, _context);
            return View(await _context.Evaluation.Where(e => e.SchoolName == schoolName && e.LectiveYear != actualLectiveYear).ToListAsync());
        }

        /// <summary>
        /// This method of EvaluationsController returns the Dictionary of CourseKey and floatValue
        /// that contains the grade average of each course based on the evaluations already created,
        /// of the logged user school, of the actual lective year.
        /// </summary>
        /// <returns>Dictionary of CourseKey and floatValue coursesGradesAverage</returns>
        public async Task<Dictionary<Course, float>> CoursesAverageAsync()
        {
            var map = new Dictionary<Course, float>();
            var schoolName = Utils.getSchoolName(User, _context);
            var lectiveYear = Utils.getLectiveYear(User, _context);


            List<int> coursesIdInEvaluations = new List<int>();
            List<Course> coursesInEvaluations = new List<Course>();
            foreach (Evaluation e in await _context.Evaluation.Where(u => u.SchoolName == schoolName && u.LectiveYear == lectiveYear).ToListAsync())
            {
                if (!coursesIdInEvaluations.Contains(e.CourseId))
                {
                    coursesIdInEvaluations.Add(e.CourseId);
                    coursesInEvaluations.Add(await _context.Course.FirstOrDefaultAsync(c => c.CourseId == e.CourseId));
                }
            }

            foreach (Course c in coursesInEvaluations)
            {
                var sum = 0; int i = 0;
                foreach (Evaluation e in await _context.Evaluation.Where(u => u.SchoolName == schoolName && u.CourseId == c.CourseId && u.LectiveYear ==lectiveYear).ToListAsync())
                {
                    foreach (Grade g in await _context.Grade.Where(u => u.SchoolName == schoolName && u.EvaluationID == e.EvaluationId && u.LectiveYear == lectiveYear).ToListAsync())
                    {
                        sum += g.GradeNumber;
                        i++;
                    }
                }
                map.Add(c, (float)sum / (float)i);
            }
            return map;
        }

        // GET: Evaluations/InitialCreate
        /// <summary>
        /// This method of EvaluationsController returns the InitialCreate View
        /// that shows the form of the first step to create a new Evaluation.
        /// </summary>
        /// <returns>View()</returns>
        public IActionResult InitialCreate()
        {
            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            return View();
        }

        // POST: Evaluations/InitialCreate
        /// <summary>
        /// This method of EvaluationsController returns the InitialCreate View
        /// that shows the form of the first step to create a new Evaluation.
        /// </summary>
        /// <param name="evaluation">Evaluation evaluation form info</param>
        /// <returns>View(Evaluation evaluationFormInfo)</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InitialCreate(Evaluation evaluation)
        {
            if (evaluation.CourseId.Equals("null"))
            {
                ModelState.AddModelError(string.Empty, "É obrigatório a seleção do Curso.");
            }
            else if (ModelState.IsValid)
            {
                return RedirectToAction(nameof(GetClassesCreate), new { courseID = evaluation.CourseId });
            }

            ViewBag.courses = Utils.GetListOfCourses(User, _context);
            return View(evaluation);
        }

        /// <summary>
        /// This method returns a SelectListItem List, for dropdown selectors purposes,
        /// that contains the school classes of the given course id, of the logged user school
        /// and if this logged user is the ClassDirector of school classes.
        /// </summary>
        /// <param name="courseID">Course ID</param>
        /// <returns>List<SelectListItem> of school classes of the given course id of logged user school</returns>
        public List<SelectListItem> GetListOfSchoolClassesOfDT(int courseID)
        {
            List<SelectListItem> listSchoolClasses = new List<SelectListItem>();
            foreach (SchoolClass sc in _context.SchoolClass.Where(scs => scs.isDeleted == false && scs.Students != null && scs.CourseId == courseID && scs.SchoolName == Utils.getSchoolName(User, _context) && scs.LectiveYear == Utils.getLectiveYear(User, _context) && scs.TeacherId == Utils.getLoggedUser(User, _context).Id).ToList())
            {
                listSchoolClasses.Add(new SelectListItem { Text = sc.Name, Value = sc.SchoolClassId.ToString() });
            }
            return listSchoolClasses;
        }

        // GET: Evaluations/GetClassesCreate
        /// <summary>
        /// This method of EvaluationsController returns the GetClassesCreate View
        /// that shows the form of the final step to create a new Evaluation.
        /// </summary>
        /// <param name="courseID">int courseId to find the course to create the evaluation</param>
        /// <returns>View()</returns>
        public IActionResult GetClassesCreate(int courseID)
        {
            ViewBag.schoolClasses = GetListOfSchoolClassesOfDT(courseID);
            return View();
        }

        // POST: Evaluations/GetClassesCreate
        /// <summary>
        /// This async method of EvaluationsController redirects to the Create View
        /// of the GradesController that is used to create the grades for this evaluation
        /// if the evaluation was well created, otherwise returns the GetClassesCreate View
        /// that shows the form of the final step to create a new Evaluation, with the respective errors.
        /// </summary>
        /// <param name="evaluation">Evaluation evaluation form info</param>
        /// <returns>View depending if the evaluation was well created or not.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetClassesCreate(Evaluation evaluation)
        {
            if (evaluation.SchoolClassId.Equals("null"))
            {
                ModelState.AddModelError(string.Empty, "É obrigatório a seleção da Turma.");
            }
            else if (ModelState.IsValid)
            {
                var actualLectiveYear = Utils.getLectiveYear(User, _context);
                var schoolName = Utils.getSchoolName(User, _context);
                var exists = _context.Evaluation.FirstOrDefault(e => e.CourseId == evaluation.CourseId && e.SchoolClassId == evaluation.SchoolClassId && e.SchoolName == schoolName && e.LectiveYear == actualLectiveYear);
                if (exists != null)
                {
                    ModelState.AddModelError(string.Empty, "Esta avaliação já foi criada.");
                    return RedirectToAction(nameof(Index));                    
                }

                //Teacher/DT neste caso Id será o do user loggado
                evaluation.TeacherId = Utils.getLoggedUser(User, _context).Id;
                evaluation.SchoolName = schoolName;
                evaluation.LectiveYear = actualLectiveYear;
                _context.Add(evaluation);
                await _context.SaveChangesAsync();
                return RedirectToAction("Create", "Grades", new { courseID = evaluation.CourseId, classID = evaluation.SchoolClassId });
            }

            ViewBag.schoolClasses = Utils.GetListOfSchoolClasses(evaluation.CourseId, User, _context);
            return View(evaluation);
        }

        // GET: Evaluations/Delete/5
        /// <summary>
        /// This async method of EvaluationsController returns the Delete View
        /// that is used to delete the evaluation of the given id.
        /// </summary>
        /// <param name="id">string id of the evaluation to delete</param>
        /// <returns>View(Evaluation evaluationToDelete)</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            var evaluation = await _context.Evaluation.FirstOrDefaultAsync(m => m.EvaluationId == id);
            if (evaluation == null) return NotFound();

            return View(evaluation);
        }

        // POST: Evaluations/Delete/5
        /// <summary>
        /// This async method of EvaluationsController redirects to the Index View that shows 
        /// all the active evaluations of the logged user school, of the actual lective year. <br/>
        /// This method is used to delete the evaluation of the given id.
        /// </summary>
        /// <param name="id">string id of the evaluation to delete</param>
        /// <returns>Redirects to the Index View</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evaluation = await _context.Evaluation.FindAsync(id);

            var grades = _context.Grade.Where(g => g.EvaluationID == evaluation.EvaluationId);
            _context.Grade.RemoveRange(grades);

            _context.Evaluation.Remove(evaluation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// This method of EvaluationsController returns true if exists a evaluation
        /// with the given id, otherwise returns false.
        /// </summary>
        /// <param name="id">string Id of the evaluation we want to find</param>
        /// <returns>true if the evaluation exists, otherwise returns false</returns>
        private bool EvaluationExists(int id)
        {
            return _context.Evaluation.Any(e => e.EvaluationId == id);
        }

    }
}
