﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace GEscola.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<Person> _signInManager;
        private readonly UserManager<Person> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        public RegisterModel(
            ApplicationDbContext context,
            UserManager<Person> userManager,
            SignInManager<Person> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
            [DataType(DataType.Text)]
            [Display(Name = "Nome")]
            public string Name { get; set; }

            [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
            [DataType(DataType.Text)]
            [Display(Name = "Nome da Escola")]
            public string SchoolName { get; set; }

            [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
            [DataType(DataType.Text)]
            [Display(Name = "Nome do Ano Letivo (Ex: 2020/2021)")]
            public string LectiveYearName { get; set; }

            [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
            [StringLength(100, ErrorMessage = "A nova password deve de conter pelo menos {2} caracteres.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Required(ErrorMessage = "O campo \"{0}\" é obrigatório.")]
            [DataType(DataType.Password)]
            [Display(Name = "Confirmar password")]
            [Compare("Password", ErrorMessage = "As passwords inseridas não coincidem.")]
            public string ConfirmPassword { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var admins = _context.Users.Where(user => user.Name == "Admin");
                bool existsAdminSchool = admins.Any(admin => admin.SchoolName == Input.SchoolName);
                if (existsAdminSchool)
                {
                    ModelState.AddModelError(string.Empty, "Já existe um administrador para esta escola.");
                    return Page();
                }

                var user = new Person { Name = Input.Name, UserName = Input.Email, Email = Input.Email, SchoolName = Input.SchoolName, LectiveYearName = Input.LectiveYearName };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                     await _userManager.AddToRoleAsync(user, "Admin");
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "GEscola - Confirmar Registo",
                        $"Olá, <br/>Para que possa confirmar o seu registo na plataforma GEscola <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clique aqui</a>.<br/>Atenciosamente,<br/>GEscola");

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
