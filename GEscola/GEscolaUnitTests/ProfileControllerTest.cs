﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class ProfileControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private ProfileController _controller;


        public ProfileControllerTest(ApplicationDbContextFixture contextFixture)
        {

            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            var users = _context.Users;

            var fakeUserManager = new Mock<FakeUserManager>();
            fakeUserManager.Setup(x => x.Users).Returns(users);
            fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<Person>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);

            var signInManager = new Mock<FakeSignInManager>();
            signInManager.Setup(x => x.PasswordSignInAsync(
                It.IsAny<Person>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>())).ReturnsAsync(Microsoft.AspNetCore.Identity.SignInResult.Success);

            _controller = new ProfileController(_context, fakeUserManager.Object, signInManager.Object);
        }

        [Fact]
        public void getSchoolAverage_returnsFloat()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };
            var result = _controller.GetSchoolAverage();

            Assert.Equal(14.5, result);
        }

        [Fact]
        public void getSelfAverage_returnsFloat()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "16cb65be-d5b5-4622-b4ad-ba43c61ab63f"),
                new Claim(ClaimTypes.Name, "180221125@estudantes.ips.pt"),
                new Claim(ClaimTypes.Role, "Student")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };
            var result = _controller.GetSelfAverage();

            Assert.Equal(13.5, result);
        }

        [Fact]
        public void Profile_returnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Profile("180221125 estudantes^ips^pt");

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void ChangePassword_returnsViewResult()
        {
            var result = _controller.ChangePassword();

            Assert.IsType<ViewResult>(result);
        }
    }
}
