﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class SchoolClassesControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private SchoolClassesController _controller;


        public SchoolClassesControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            var users = _context.Users;

            var fakeUserManager = new Mock<FakeUserManager>();
            fakeUserManager.Setup(x => x.Users).Returns(users);
            fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<Person>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);


            _controller = new SchoolClassesController(_context, fakeUserManager.Object);
        }
       

        [Fact]
        public async Task Index_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<SchoolClassOP>>(viewResult.ViewData.Model).Where(u => u.SchoolClass.isDeleted == false && u.SchoolClass.SchoolName == "IPS - ESTSetúbal");
            Assert.NotNull(model);

        }
        
        [Fact]
        public async Task Details_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Details(1);

            Assert.IsType<ViewResult>(result);

        }
        
        [Fact]
        public void GetListOfTeachers_ReturnsListCheckBox()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.GetListOfTeachers();
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Nuno Pina", Value = "2d0f2874-7df0-4d13-8f47-5e6c008c89ab" });

            Assert.Equal(list.ToString(), result.ToString());
        }


        [Fact]
        public void GetTeacherName_GivenId_ReturnsString()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.GetTeacherName("2d0f2874-7df0-4d13-8f47-5e6c008c89ab");
            
            Assert.Equal("Nuno Pina", result);
        }

        [Fact]
        public void GetCourseName_GivenId_ReturnsString()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.GetCourseName(1);

            Assert.Equal("LEI", result);
        }

        [Fact]
        public void ConvertSchoolClassToOP_ListSchoolClasses_ReturnsListSchoolClassOP()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };


            List<SchoolClass> list = new List<SchoolClass>();
            list.Add(_fakeDb.GetContext().SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == 1));

            var result = _controller.ConvertSchoolClassToOP(list);


            List<SchoolClassOP> listOP = new List<SchoolClassOP>();
            listOP.Add(new SchoolClassOP { SchoolClass = _fakeDb.GetContext().SchoolClass.FirstOrDefault(sc => sc.SchoolClassId == 1), CourseName = "LEI", TeacherName = "Nuno Pina" });

            Assert.Equal(listOP.ToString(), result.ToString());
        }

      [Fact]
        public void Create_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Create();

            Assert.IsType<ViewResult>(result);

        }

       [Fact]
        public async Task Create_GivenSchoolClass_ReturnsRedirectTo()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            SchoolClass sc = new SchoolClass()
            {
                SchoolClassId = 3,
                Name = "SW03",
                SchoolName = "IPS - ESTSetúbal",
                CourseId = 1,
                TeacherId = "2d0f2874-7df0-4d13-8f47-5e6c008c89ab",
                Students = "180221125@estudantes.ips.pt|190221120@estudantes.ips.pt|",
                isDeleted = false
            };

            var result = await _controller.Create(sc);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void Edit_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Edit(2, null);

            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public async Task RemoveStudent_GivenIdStudentEmail_ReturnsViewResult()
        {

            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.RemoveStudent(1, "190221120@estudantes.ips.pt");

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async Task AddStudent_GivenIdStudentEmail_ReturnsViewResult()
        {

            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.AddStudent(1, "190221120@estudantes.ips.pt");

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async Task Edit_GivenNonExistingId_ReturnsNotFound()
        {

            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Edit(9, "2d0f2874-7df0-4d13-8f47-5e6c008c89ab");

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Delete(2);

            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public void DeleteConfirmed_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.DeleteConfirmed(2);

            Assert.IsType<Task<IActionResult>>(result);
        }
    }
}
