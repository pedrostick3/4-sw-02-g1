﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class EvaluationsControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private EvaluationsController _controller;


        public EvaluationsControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            _controller = new EvaluationsController(_context);
        }

        [Fact]
        public void InitialCreate_GivenEvaluation_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fc8fce04-c029-4416-b5dd-7cc7198deb40"),
                new Claim(ClaimTypes.Name, "joao.ratao@ejaf.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.InitialCreate(_fakeDb.GetContext().Evaluation.FirstOrDefault(e => e.EvaluationId == 2));
            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void GetClassesCreate_GivenEvaluation_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fc8fce04-c029-4416-b5dd-7cc7198deb40"),
                new Claim(ClaimTypes.Name, "joao.ratao@ejaf.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.GetClassesCreate(_fakeDb.GetContext().Evaluation.FirstOrDefault(e => e.EvaluationId == 2));
            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public void Delete_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Delete(2);

            Assert.IsType<Task<IActionResult>>(result);
        }
        
        [Fact]
        public void DeleteConfirmed_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.DeleteConfirmed(2);

            Assert.IsType<Task<IActionResult>>(result);
        }
    }
}
