﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class CoursesControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private CoursesController _controller;


        public CoursesControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            _controller = new CoursesController(_context);
        }

      
        [Fact]
        public async Task Index_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Course>>(viewResult.ViewData.Model).Where(u => u.isDeleted == false && u.SchoolName == "IPS - ESTSetúbal");
            Assert.NotNull(model);
        }

        [Fact]
        public async Task Details_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Details(1);

            Assert.IsType<ViewResult>(result);
        }
  
        [Fact]
        public void GetListOfCoursesTypes_ReturnsListSelectListItem()
        {
            var result = _controller.GetListOfCoursesTypes();

            List<SelectListItem> listT = new List<SelectListItem>();
            listT.Add(new SelectListItem { Text = "Curso Profissional", Value = CourseType.Profissional.ToString() });
            listT.Add(new SelectListItem { Text = "Curso Regular", Value = CourseType.Regular.ToString() });
            
            Assert.Equal(listT.ToString(), result.ToString());
        }

       [Fact]
        public void GetListOfUCs_ReturnsListCheckBox()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.GetListOfUCs();

            List<Checkbox> list = new List<Checkbox>();
            list.Add(new Checkbox { IsChecked = false, Name = "PV", Value = "1" });
            list.Add(new Checkbox { IsChecked = false, Name = "ESA", Value = "2" });

            Assert.Equal(list.ToString(), result.ToString());
        }

       [Fact]
        public void Create_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Create();

            Assert.IsType<ViewResult>(result);

        }

         [Fact]
        public async Task Create_GivenCourseOP_ReturnsRedirectTo()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            CourseOP opCourse = new CourseOP();
            opCourse.Course = new Course()
            {
                CourseId = 3,
                Name = "Desporto",
                Description = "Curso de Desporto",
                CourseType = (CourseType)0,
                SchoolName = "IPS - ESTSetúbal",
                UCs = "1|",
                CourseDuration = 3,
                isDeleted = false
            };
            opCourse.ListCourseUCs = _controller.StringToList("1|"); 

            var result = await _controller.Create(opCourse);

            Assert.IsType<RedirectToActionResult>(result);
        }


        [Fact]
        public void Edit_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Edit(2);

            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public async Task Edit_GivenId_ReturnsViewResult()
        {

            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            _fakeDb.GetContext().Course.Add(new Course()
            {
                CourseId = 4,
                Name = "Desporto",
                Description = "Curso de Desporto",
                CourseType = (CourseType)0,
                SchoolName = "IPS - ESTSetúbal",
                UCs = "1|",
                CourseDuration = 3,
                isDeleted = false
            });
            _fakeDb.GetContext().SaveChanges();

            CourseOP opCourse = new CourseOP();
            opCourse.Course = _fakeDb.GetContext().Course.FirstOrDefault(c => c.CourseId == 3);
            opCourse.ListCourseUCs = _controller.StringToList("1|");

            opCourse.Course.Description = "Curso de Desporto!";

            var result = await _controller.Edit(4, opCourse);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Delete(2);

            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public void DeleteConfirmed_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.DeleteConfirmed(2);

            Assert.IsType<Task<IActionResult>>(result);
        }
    }
}
