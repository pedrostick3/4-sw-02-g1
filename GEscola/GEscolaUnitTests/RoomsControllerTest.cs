﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class RoomsControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private RoomsController _controller;


        public RoomsControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            var users = _context.Users;

            var fakeUserManager = new Mock<FakeUserManager>();
            fakeUserManager.Setup(x => x.Users).Returns(users);
            fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<Person>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);


            _controller = new RoomsController(_context);
        }
       

        [Fact]
        public async Task Index_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Room>>(viewResult.ViewData.Model).Where(u => u.isDeleted == false && u.SchoolName == "IPS - ESTSetúbal");
            Assert.NotNull(model);

        }
        
        [Fact]
        public async Task Details_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Details(1);

            Assert.IsType<ViewResult>(result);

        }
        
      

      [Fact]
        public void Create_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Create();

            Assert.IsType<ViewResult>(result);

        }

         [Fact]
        public async Task Create_GivenRoom_ReturnsRedirectTo()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            Room r = new Room()
            {
                RoomId = 3,
                SchoolName = "IPS - ESTSetúbal",
                Name = "C14",
                MaxCapacity = 30,
                Size = 10,
                Description = "Sala de Mesas individuais com computadores nas filas laterais",
                isDeleted = false
            };

            var result = await _controller.Create(r);

            Assert.IsType<RedirectToActionResult>(result);
        }

       [Fact]
        public void Edit_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Edit(2);

            Assert.IsType<Task<IActionResult>>(result);
        }          

        [Fact]
        public async Task Edit_GivenIdRoom_ReturnsRedirect()
        {

            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            Room r = _fakeDb.GetContext().Room.FirstOrDefault(r => r.RoomId == 1);

            var result = await _controller.Edit(1, r);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void Delete_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Delete(2);

            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public void DeleteConfirmed_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.DeleteConfirmed(2);

            Assert.IsType<Task<IActionResult>>(result);
        }
    }
}
