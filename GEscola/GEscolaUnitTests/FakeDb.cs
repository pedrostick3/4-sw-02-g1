﻿using GEscola.Data;
using GEscola.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xunit;

namespace GEscolaTests
{
    public class ApplicationDbContextFixture
    {
        public ApplicationDbContext DbContext { get; private set; }

        public ApplicationDbContextFixture()
        {
            //criar db
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;
            DbContext = new ApplicationDbContext(options);
            DbContext.Database.EnsureCreated();

        }
    }

    public class FakeDb : IClassFixture<ApplicationDbContextFixture>
    {
        private ApplicationDbContext _context;

        public FakeDb(ApplicationDbContextFixture contextFixture)
        {
            _context = contextFixture.DbContext;

            PopulateRoles();
            PopulateUsers();
            PopulateRolesUsers();
            PopulateUCs();
            PopulateCourses();
            PopulateSchoolClasses();
            PopulateEvaluations();
            PopulateGrades();
            PopulateRooms();
            PopulateClassRooms();
            PopulateAbsences();
        }

        public void PopulateRoles()
        {
            var role = _context.Roles.FirstOrDefaultAsync(role => role.Id == "2b98fa69-3334-4ae5-8a0a-4369df45a596");
            if (role.Result == null)
            {
                _context.Roles.Add(
                new IdentityRole()
                {
                    Id = "2b98fa69-3334-4ae5-8a0a-4369df45a596",
                    Name = "Student",
                    NormalizedName = "STUDENT",
                    ConcurrencyStamp = "0d5d1305-4e15-4054-8f90-fbbb4ef5948e"
                });
            }

            role = _context.Roles.FirstOrDefaultAsync(role => role.Id == "8e13d705-b5b7-4c9b-bd05-f29d79feb04e");
            if (role.Result == null)
            {
                _context.Roles.Add(
                new IdentityRole()
                {
                    Id = "8e13d705-b5b7-4c9b-bd05-f29d79feb04e",
                    Name = "Teacher",
                    NormalizedName = "TEACHER",
                    ConcurrencyStamp = "72f57084-a369-4359-9f89-e7dfe759a06a"
                });
            }

            role = _context.Roles.FirstOrDefaultAsync(role => role.Id == "b71f0c7a-8cf1-47ba-8de1-e14653a8b202");
            if (role.Result == null)
            {
                _context.Roles.Add(
                new IdentityRole()
                {
                    Id = "b71f0c7a-8cf1-47ba-8de1-e14653a8b202",
                    Name = "ClassDirector",
                    NormalizedName = "CLASSDIRECTOR",
                    ConcurrencyStamp = "bdbdffad-f2e1-4039-a1ab-95b86e5521a6"
                });
            }

           role = _context.Roles.FirstOrDefaultAsync(role => role.Id == "c0caf471-3094-4eb3-a5cb-ad53c0c2b90d");
            if (role.Result == null)
            {
                _context.Roles.Add(
                new IdentityRole()
                {
                    Id = "c0caf471-3094-4eb3-a5cb-ad53c0c2b90d",
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    ConcurrencyStamp = "a49b2d80-f5f9-476e-874e-7a3fa21fa1ae"
                });
            }
            _context.SaveChanges();
        }

        public void PopulateUsers()
        {
            var user = _context.Users.FirstOrDefaultAsync(user => user.Id == "16cb65be-d5b5-4622-b4ad-ba43c61ab63f");
            if(user.Result == null)
            {
                _context.Users.Add(
                new Person()
                {
                    Id = "16cb65be-d5b5-4622-b4ad-ba43c61ab63f",
                    Birthdate = new DateTime(1998, 5, 5),
                    UserName = "180221125@estudantes.ips.pt",
                    NormalizedUserName = "180221125@ESTUDANTES.IPS.PT",
                    Email = "180221125@estudantes.ips.pt",
                    NormalizedEmail = "180221125@ESTUDANTES.IPS.PT",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEIanUf0FKFr722oIBBLylhCCYB6/omj7cDN01NClf2ViquO3vkIUuXCNuf7bg6gNvA==",
                    SecurityStamp = "7TERFLDMRXHVPJAZLCU3CAPQLURM3VC4",
                    ConcurrencyStamp = "28f1d864-c713-4682-b74e-93b9a9a9683d",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnd = null,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                    Name = "Catarina Pedrosa",
                    SchoolName = "IPS - ESTSetúbal",
                    SecondaryEmail = "catarinabp5@gmail.com",
                    ParentalPIN = 1234
                });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "42d2e815-4317-4680-b4c7-46bdc1e6294e");
            if (user.Result == null)
            {
                _context.Users.Add(
                new Person()
                {
                    Id = "42d2e815-4317-4680-b4c7-46bdc1e6294e",
                    Birthdate = new DateTime(2010, 7, 23),
                    UserName = "190221120@estudantes.ips.pt",
                    NormalizedUserName = "190221120@ESTUDANTES.IPS.PT",
                    Email = "190221120@estudantes.ips.pt",
                    NormalizedEmail = "190221120@ESTUDANTES.IPS.PT",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEG19cNoArUduiFEG9mc/SH+n5ZW4wOLOVwLYM++racGdnKOlHyoc64SCwl7J8tNkog==",
                    SecurityStamp = "QJ4NXRHK6LKTEH2Y26MCU2SK62M336NL",
                    ConcurrencyStamp = "c52a46f0-f2ca-4ab7-b513-c3b1121c29a9",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnd = null,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                    Name = "Pedro Dionísio",
                    SchoolName = "IPS - ESTSetúbal",
                    SecondaryEmail = "pedrostick3@gmail.com",
                    ParentalPIN = 1234
                });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "ee3d5706-9cd7-430c-ac81-7b377ec7d31d");
            if (user.Result == null)
            {
            _context.Users.Add(
                new Person()
                {
                    Id = "ee3d5706-9cd7-430c-ac81-7b377ec7d31d",
                    UserName = "admin@ips.pt",
                    NormalizedUserName = "ADMIN@IPS.PT",
                    Email = "admin@ips.pt",
                    NormalizedEmail = "ADMIN@IPS.PT",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEC9e+rkT5x9WgPe4X2aWuO+yApjxX4BjuYDUN0IeqMQS28edMcBdWHK1TSnA7xNhrQ==",
                    SecurityStamp = "SFH7LTTGO4UWLOVYNYD7WDEB5ZNRH7Q3",
                    ConcurrencyStamp = "40361d20-079b-415e-98f0-4847885c3861",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnd = null,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                    Name = "Admin",
                    SchoolName = "IPS - ESTSetúbal",
                    SecondaryEmail = null,
                    ParentalPIN = 0
                });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2");
            if (user.Result == null)
            {
                _context.Users.Add(
                new Person()
                {
                    Id = "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2",
                    UserName = "jose.cordeiro@estsetubal.ips.pt",
                    NormalizedUserName = "JOSE.CORDEIRO@ESTSETUBAL.IPS.PT",
                    Email = "jose.cordeiro@estsetubal.ips.pt",
                    NormalizedEmail = "JOSE.CORDEIRO@ESTSETUBAL.IPS.PT",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEKwCgk5c0zfMvIgRGtQLUU7KhABz/Vy0KYtW/hyuj5Wpl1Geto2OQ1Y780YKTXYMJg==AQAAAAEAACcQAAAAEIanUf0FKFr722oIBBLylhCCYB6/omj7cDN01NClf2ViquO3vkIUuXCNuf7bg6gNvA==",
                    SecurityStamp = "NNL4V4KYIIXVTCUJSHIFVB2FBOHS5I2Q",
                    ConcurrencyStamp = "58cc8476-d443-4e11-a97e-66d17de2b21d",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnd = null,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                    Name = "José Cordeiro",
                    SchoolName = "IPS - ESTSetúbal",
                    SecondaryEmail = null,
                    ParentalPIN = 0
                });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "2d0f2874-7df0-4d13-8f47-5e6c008c89ab");
            if (user.Result == null)
            {
                _context.Users.Add(
                new Person()
                {
                    Id = "2d0f2874-7df0-4d13-8f47-5e6c008c89ab",
                    UserName = "nuno.pina@ips.pt",
                    NormalizedUserName = "NUNO.PINA@IPS.PT",
                    Email = "nuno.pina@ips.pt",
                    NormalizedEmail = "NUNO.PINA@IPS.PT",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEMF0wmGbgKu+UqXZrkJytqDdgEko0orPPMkFPjCPnDSIoSsA7bzsSIPSBDKb7PTX7g==",
                    SecurityStamp = "EN6VJENZPZ7IGBDSVYQ3DSWFJ2I4NGX7",
                    ConcurrencyStamp = "11688225-d1ac-4d22-9317-2695a4be5e4e",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnd = null,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                    Name = "Nuno Pina",
                    SchoolName = "IPS - ESTSetúbal",
                    SecondaryEmail = null,
                    ParentalPIN = 0
                });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "fb2d77b5-5591-4a6b-ad82-e0164ed66350");
            if (user.Result == null)
            {
                _context.Users.Add(
                    new Person()
                    {
                        Id = "fb2d77b5-5591-4a6b-ad82-e0164ed66350",
                        UserName = "admin@ejaf.pt",
                        NormalizedUserName = "ADMIN@EJAF.PT",
                        Email = "admin@ejaf.pt",
                        NormalizedEmail = "ADMIN@EJAF.PT",
                        EmailConfirmed = true,
                        PasswordHash = "AQAAAAEAACcQAAAAEKE231/sDRlUnA3fVy6wQUM6r2dBZVrZYZCprnsKDqHEJSccqirSbacx1/dDfHNeFQ==",
                        SecurityStamp = "LSNRTQOSOCA53L72D7W4WRXPKT57GUXV",
                        ConcurrencyStamp = "757d4adb-8714-4424-91d0-f325971e4aa3",
                        PhoneNumber = null,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnd = null,
                        LockoutEnabled = true,
                        AccessFailedCount = 0,
                        Name = "Administrador",
                        SchoolName = "EJAF - Externato João Alberto Faria",
                        SecondaryEmail = null,
                        ParentalPIN = 0
                    });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "fc8fce04-c029-4416-b5dd-7cc7198deb40");
            if (user.Result == null)
            {
                _context.Users.Add(
                    new Person()
                    {
                        Id = "fc8fce04-c029-4416-b5dd-7cc7198deb40",
                        UserName = "joao.ratao@ejaf.pt",
                        NormalizedUserName = "JOAO.RATAO@EJAF.PT",
                        Email = "joao.ratao@ejaf.pt",
                        NormalizedEmail = "JOAO.RATAO@EJAF.PT",
                        EmailConfirmed = true,
                        PasswordHash = "AQAAAAEAACcQAAAAEIFMA1Wjbl+yoeHCD3D8EFARHyHvXnd33J6et5I/lTHoFpVZRbor/s6z5oyLgzcp4A==",
                        SecurityStamp = "4IIDYSQ4Y2YNM2PJP4XGZ47DPG3JC3JV",
                        ConcurrencyStamp = "a7cec687-981a-4cc5-a3fe-f97697b06d16",
                        PhoneNumber = null,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnd = null,
                        LockoutEnabled = true,
                        AccessFailedCount = 0,
                        Name = "João Ratão",
                        SchoolName = "EJAF - Externato João Alberto Faria",
                        SecondaryEmail = null,
                        ParentalPIN = 0
                    });
            }

            user = _context.Users.FirstOrDefaultAsync(user => user.Id == "92357ba4-02c1-4a5d-b6fb-070986b31877");
            if (user.Result == null)
            {
                _context.Users.Add(
                    new Person()
                    {
                        Id = "92357ba4-02c1-4a5d-b6fb-070986b31877",
                        UserName = "ze.falido@ejaf.pt",
                        NormalizedUserName = "ZE.FALIDO@EJAF.PT",
                        Email = "ze.falido@ejaf.pt",
                        NormalizedEmail = "ZE.FALIDO@EJAF.PT",
                        EmailConfirmed = true,
                        PasswordHash = "AQAAAAEAACcQAAAAEEESedaFQOV5sJLXeJuldOB59W1h+fWijayCDiH3Lk8txMv1mvIn0Pfq8LR1/A3xCw==",
                        SecurityStamp = "XXHGFWNZ5Z5KHHFJSRRHKXWCY5PVRLZ6",
                        ConcurrencyStamp = "7bde7c74-aa7c-4623-bfbf-3109591b2e5c",
                        PhoneNumber = null,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnd = null,
                        LockoutEnabled = true,
                        AccessFailedCount = 0,
                        Name = "Zé Falido",
                        SchoolName = "EJAF - Externato João Alberto Faria",
                        SecondaryEmail = null,
                        ParentalPIN = 0
                    });
            }

            _context.SaveChanges();
        }

        public void PopulateRolesUsers()
        {
            //Student IPS (Catarina)
            var userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "16cb65be-d5b5-4622-b4ad-ba43c61ab63f" && ur.RoleId == "2b98fa69-3334-4ae5-8a0a-4369df45a596");
            if(userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "16cb65be-d5b5-4622-b4ad-ba43c61ab63f",
                    RoleId = "2b98fa69-3334-4ae5-8a0a-4369df45a596"
                });
            }

            //Student IPS (Pedro)
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "42d2e815-4317-4680-b4c7-46bdc1e6294e" && ur.RoleId == "2b98fa69-3334-4ae5-8a0a-4369df45a596");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "42d2e815-4317-4680-b4c7-46bdc1e6294e",
                    RoleId = "2b98fa69-3334-4ae5-8a0a-4369df45a596"
                });
            }

            //Student EJAF (Zé)
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "92357ba4-02c1-4a5d-b6fb-070986b31877" && ur.RoleId == "2b98fa69-3334-4ae5-8a0a-4369df45a596");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "92357ba4-02c1-4a5d-b6fb-070986b31877",
                    RoleId = "2b98fa69-3334-4ae5-8a0a-4369df45a596"
                });
            }

            //Admin IPS
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "ee3d5706-9cd7-430c-ac81-7b377ec7d31d" && ur.RoleId == "c0caf471-3094-4eb3-a5cb-ad53c0c2b90d");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "ee3d5706-9cd7-430c-ac81-7b377ec7d31d",
                    RoleId = "c0caf471-3094-4eb3-a5cb-ad53c0c2b90d"
                });
            }

            //Admin EJAF
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "fb2d77b5-5591-4a6b-ad82-e0164ed66350" && ur.RoleId == "c0caf471-3094-4eb3-a5cb-ad53c0c2b90d");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "fb2d77b5-5591-4a6b-ad82-e0164ed66350",
                    RoleId = "c0caf471-3094-4eb3-a5cb-ad53c0c2b90d"
                });
            }

            //ClassDirector IPS (Prof. José Cordeiro)
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2" && ur.RoleId == "b71f0c7a-8cf1-47ba-8de1-e14653a8b202");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2",
                    RoleId = "b71f0c7a-8cf1-47ba-8de1-e14653a8b202"
                });
            }

            //ClassDirector EJAF (Prof. João Ratão)
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "fc8fce04-c029-4416-b5dd-7cc7198deb40" && ur.RoleId == "b71f0c7a-8cf1-47ba-8de1-e14653a8b202");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "fc8fce04-c029-4416-b5dd-7cc7198deb40",
                    RoleId = "b71f0c7a-8cf1-47ba-8de1-e14653a8b202"
                });
            }

            //Teacher IPS (Prof. Nuno)
            userRole = _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == "2d0f2874-7df0-4d13-8f47-5e6c008c89ab" && ur.RoleId == "8e13d705-b5b7-4c9b-bd05-f29d79feb04e");
            if (userRole.Result == null)
            {
                _context.UserRoles.Add(
                new IdentityUserRole<string>()
                {
                    UserId = "2d0f2874-7df0-4d13-8f47-5e6c008c89ab",
                    RoleId = "8e13d705-b5b7-4c9b-bd05-f29d79feb04e"
                });
            }
            _context.SaveChanges();
        }
        
        public void PopulateUCs()
        {
            var uc = _context.UC.FirstOrDefaultAsync(uc => uc.Name == "PV");
            if(uc.Result == null)
            {
                _context.UC.Add(new UC()
                {
                    UCId = 1,
                    SchoolName = "IPS - ESTSetúbal",
                    Name = "PV",
                    TeacherId = "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2",
                    Description = "Programação Visual",
                    TeacherName = "José Cordeiro"
                });
            }

            uc = _context.UC.FirstOrDefaultAsync(uc => uc.Name == "ESA");
            if (uc.Result == null)
            {
                _context.UC.Add(new UC()
                {
                    UCId = 2,
                    SchoolName = "IPS - ESTSetúbal",
                    Name = "ESA",
                    TeacherId = "62d0f2874-7df0-4d13-8f47-5e6c008c89ab",
                    Description = "Engenharia de Software Aplicada",
                    TeacherName = "Nuno Pina"
                });
            }
            
            uc = _context.UC.FirstOrDefaultAsync(uc => uc.Name == "PT");
            if (uc.Result == null)
            {
                _context.UC.Add(new UC()
                {
                    UCId = 3,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    Name = "PT",
                    TeacherId = "fc8fce04-c029-4416-b5dd-7cc7198deb40",
                    Description = "Português",
                    TeacherName = "João Ratão"
                });
            }

            _context.SaveChanges();
        }

        public void PopulateCourses()
        {
            var course = _context.Course.FirstOrDefaultAsync(c => c.Name == "LEI");
            if(course.Result == null)
            {
                _context.Course.Add(new Course() 
                { 
                    CourseId = 1,
                    Name = "LEI",
                    Description = "Licenciatura em Engenharia Informática",
                    CourseType = (CourseType)0,
                    SchoolName = "IPS - ESTSetúbal",
                    UCs = "1|2|",
                    CourseDuration = 3,
                    isDeleted = false
                });
            }

            course = _context.Course.FirstOrDefaultAsync(c => c.Name == "Humanidades");
            if (course.Result == null)
            {
                _context.Course.Add(new Course()
                {
                    CourseId = 2,
                    Name = "Humanidades",
                    Description = "Curso de Humanidades",
                    CourseType = (CourseType)1,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    UCs = "3|",
                    CourseDuration = 3,
                    isDeleted = false
                });
            }

            _context.SaveChanges();
        }

        public void PopulateSchoolClasses()
        {
            var sc = _context.SchoolClass.FirstOrDefaultAsync(sc => sc.Name == "SW01");
            if (sc.Result == null)
            {
                _context.SchoolClass.Add(new SchoolClass()
                {
                    SchoolClassId = 1,
                    Name = "SW01",
                    SchoolName = "IPS - ESTSetúbal",
                    CourseId = 1,
                    TeacherId = "2d0f2874-7df0-4d13-8f47-5e6c008c89ab",
                    Students = "180221125@estudantes.ips.pt|190221120@estudantes.ips.pt|",
                    isDeleted = false
                });
            }

            sc = _context.SchoolClass.FirstOrDefaultAsync(sc => sc.Name == "H1");
            if (sc.Result == null)
            {
                _context.SchoolClass.Add(new SchoolClass()
                {
                    SchoolClassId = 2,
                    Name = "H1",
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    CourseId = 2,
                    TeacherId = "fc8fce04-c029-4416-b5dd-7cc7198deb40",
                    Students = "ze.falido@ejaf.pt|",
                    isDeleted = false
                });
            }
            _context.SaveChanges();
        }

        public void PopulateEvaluations()
        {
            var sc = _context.Evaluation.FirstOrDefaultAsync(e => e.CourseId == 1 && e.SchoolClassId == 1);
            if (sc.Result == null)
            {
                _context.Evaluation.Add(new Evaluation()
                {
                    EvaluationId = 1,
                    SchoolClassId = 1,
                    SchoolName = "IPS - ESTSetúbal",
                    CourseId = 1,
                    TeacherId = "2d0f2874-7df0-4d13-8f47-5e6c008c89ab",
                    isDeleted = false
                });
            }

            sc = _context.Evaluation.FirstOrDefaultAsync(e => e.CourseId == 2 && e.SchoolClassId == 2);
            if (sc.Result == null)
            {
                _context.Evaluation.Add(new Evaluation()
                {
                    EvaluationId = 2,
                    SchoolClassId = 2,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    CourseId = 2,
                    TeacherId = "fc8fce04-c029-4416-b5dd-7cc7198deb40",
                    isDeleted = false
                });
            }

            _context.SaveChanges();
        }

        public void PopulateGrades()
        {
            var sc = _context.Grade.FirstOrDefaultAsync(g => g.EvaluationID == 1);
            if (sc.Result == null)
            {
                _context.Grade.AddRange(
                new Grade()
                {
                    GradeId = 1,
                    SchoolName = "IPS - ESTSetúbal",
                    EvaluationID = 1,
                    StudentID = "16cb65be-d5b5-4622-b4ad-ba43c61ab63f",
                    UCID = 1,
                    GradeNumber = 13
                },
                new Grade()
                {
                    GradeId = 2,
                    SchoolName = "IPS - ESTSetúbal",
                    EvaluationID = 1,
                    StudentID = "16cb65be-d5b5-4622-b4ad-ba43c61ab63f",
                    UCID = 2,
                    GradeNumber = 14
                },
                new Grade()
                {
                    GradeId = 3,
                    SchoolName = "IPS - ESTSetúbal",
                    EvaluationID = 1,
                    StudentID = "42d2e815-4317-4680-b4c7-46bdc1e6294e",
                    UCID = 1,
                    GradeNumber = 15
                },
                new Grade()
                {
                    GradeId = 4,
                    SchoolName = "IPS - ESTSetúbal",
                    EvaluationID = 1,
                    StudentID = "42d2e815-4317-4680-b4c7-46bdc1e6294e",
                    UCID = 2,
                    GradeNumber = 16
                });
            }

            sc = _context.Grade.FirstOrDefaultAsync(g => g.EvaluationID == 1);
            if (sc.Result == null)
            {

                _context.Grade.AddRange(
                new Grade()
                {
                    GradeId = 5,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    EvaluationID = 2,
                    StudentID = "92357ba4-02c1-4a5d-b6fb-070986b31877",
                    UCID = 3,
                    GradeNumber = 18
                });
            }

            _context.SaveChanges();
        }

        public void PopulateRooms()
        {
            var r = _context.Room.FirstOrDefaultAsync(r => r.RoomId == 1);
            if (r.Result == null)
            {
                _context.Room.Add(new Room()
                {
                    RoomId = 1,
                    SchoolName = "IPS - ESTSetúbal",
                    Name = "C14",
                    MaxCapacity = 30,
                    Size = 10, 
                    Description = "Sala de Mesas individuais com computadores nas filas laterais",
                    isDeleted = false
                });
            }

            r = _context.Room.FirstOrDefaultAsync(r => r.RoomId == 2);
            if (r.Result == null)
            {
                _context.Room.Add(new Room()
                {
                    RoomId = 2,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    Name = "B24",
                    MaxCapacity = 35,
                    Size = 11,
                    Description = "Sala de Mesas individuais com computadores nas filas laterais",
                    isDeleted = false
                });
            }

            _context.SaveChanges();
        }

        public void PopulateClassRooms()
        {
            var r = _context.ClassRoom.FirstOrDefaultAsync(r => r.ClassRoomId == 1);
            if (r.Result == null)
            {
                _context.ClassRoom.Add(new ClassRoom()
                {
                    ClassRoomId = 1,
                    SchoolName = "IPS - ESTSetúbal",
                    RoomId = 1,
                    Date = new DateTime(2021, 04, 12, 09, 05, 32),
                    CourseId = 1,
                    SchoolClassId = 1, 
                    UCId = 1, 
                    TeacherId = "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2",
                    StudentsPlaces = "180221125@estudantes.ips.pt:A1|190221120@estudantes.ips.pt:|",
                    isDeleted = false
                });
            }

            r = _context.ClassRoom.FirstOrDefaultAsync(r => r.ClassRoomId == 2);
            if (r.Result == null)
            {
                _context.ClassRoom.Add(new ClassRoom()
                {
                    ClassRoomId = 2,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    RoomId = 2,
                    Date = new DateTime(2021, 04, 13, 09, 03, 10),
                    CourseId = 2,
                    SchoolClassId = 2,
                    UCId = 2,
                    TeacherId = "fc8fce04-c029-4416-b5dd-7cc7198deb40",
                    StudentsPlaces = "ze.falido@ejaf.pt:|",
                    isDeleted = false
                });
            }

            _context.SaveChanges();
        }

        public void PopulateAbsences()
        {
            var a = _context.Absence.FirstOrDefaultAsync(a => a.AbsenceId == 1);
            if (a.Result == null)
            {
                _context.Absence.Add(new Absence()
                {
                    AbsenceId = 1,
                    SchoolName = "IPS - ESTSetúbal",
                    ClassRoomId = 1,
                    StudentId = "42d2e815-4317-4680-b4c7-46bdc1e6294e",
                    isDeleted = false
                });
            }

            a = _context.Absence.FirstOrDefaultAsync(a => a.AbsenceId == 2);
            if (a.Result == null)
            {
                _context.Absence.Add(new Absence()
                {
                    AbsenceId = 2,
                    SchoolName = "EJAF - Externato João Alberto Faria",
                    ClassRoomId = 2,
                    StudentId = "92357ba4-02c1-4a5d-b6fb-070986b31877",
                    isDeleted = false
                });
            }

            _context.SaveChanges();
        }



        public ApplicationDbContext GetContext()
        {
            return _context;
        }
    }
}
