﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class AbsencesControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private AbsencesController _controller;
        private readonly IEmailSender _emailSender;


        public AbsencesControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            _controller = new AbsencesController(_context, _emailSender);
        }

      
        [Fact]
        public async Task Index_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2"),
                new Claim(ClaimTypes.Name, "jose.cordeiro@estsetubal.ips.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Index();

            // Assert
            Assert.IsType<ViewResult>(result);
            ;
        }

        [Fact]
        public async Task AcceptedAbsences_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2"),
                new Claim(ClaimTypes.Name, "jose.cordeiro@estsetubal.ips.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.AcceptedAbsences();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task ParentalPIN_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2"),
                new Claim(ClaimTypes.Name, "jose.cordeiro@estsetubal.ips.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.ParentalPIN(1);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void ParentalPIN_GivenIdAbsence_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2"),
                new Claim(ClaimTypes.Name, "jose.cordeiro@estsetubal.ips.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            Absence abs = _fakeDb.GetContext().Absence.FirstOrDefault(a => a.AbsenceId == 1);

            var result = _controller.ParentalPIN(1, abs);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task Justify_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "92357ba4-02c1-4a5d-b6fb-070986b31877"),
                new Claim(ClaimTypes.Name, "ze.falido@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Student")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Justify(2);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task Justify_GivenIdAbsence_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "92357ba4-02c1-4a5d-b6fb-070986b31877"),
                new Claim(ClaimTypes.Name, "ze.falido@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Student")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            Absence abs = _fakeDb.GetContext().Absence.FirstOrDefault(a => a.AbsenceId == 2);

            var result = await _controller.Justify(2, abs);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task Accept_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2"),
                new Claim(ClaimTypes.Name, "jose.cordeiro@estsetubal.ips.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Accept(1);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async Task Decline_GivenNonExistingId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fc8fce04-c029-4416-b5dd-7cc7198deb40"),
                new Claim(ClaimTypes.Name, "joao.ratao@ejaf.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Decline(3);

            Assert.IsType<NotFoundResult>(result);
        }
    }
}
