﻿using GEscola.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace GEscolaTests
{
    public class FakeUserManager : UserManager<Person>
    {
        public FakeUserManager()
            : base(new Mock<IUserStore<Person>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<IPasswordHasher<Person>>().Object,
                  new IUserValidator<Person>[0],
                  new IPasswordValidator<Person>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<IServiceProvider>().Object,
                  new Mock<ILogger<UserManager<Person>>>().Object)
        { }

    }
}
