﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class UCsControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private UCsController _controller;


        public UCsControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            _controller = new UCsController(_context);
        }
        
        [Fact]
        public async Task Index_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<UC>>(viewResult.ViewData.Model).Where(u => u.isDeleted == false && u.SchoolName == "IPS - ESTSetúbal");
            Assert.NotNull(model);
        }
        
        [Fact]
        public async Task Details_GivenId_ReturnsViewResult()
        {


            var result = await _controller.Details(1);

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(_fakeDb.GetContext().UC.FirstOrDefault(u => u.UCId == 1), viewResult.ViewData.Model);
        }

        [Fact]
        public void Create_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Create();

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void GetResponsibleTeacherName_GivenId_ReturnsName()
        {
            var result = _controller.GetResponsibleTeacherName("66c86a13-0ce5-441c-8e5c-c7e1e8b19ce2");

            Assert.Equal("José Cordeiro", result);
        }

        [Fact]
        public void Edit_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Administrador")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Edit(3);

            Assert.IsType<Task<IActionResult>>(result);
        }
    }
}
