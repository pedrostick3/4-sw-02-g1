﻿using GEscola.Controllers;
using GEscola.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GEscolaTests
{
    public class ClassRoomsControllerTest : IClassFixture<ApplicationDbContextFixture>
    {
        private FakeDb _fakeDb;
        private ClassRoomsController _controller;
        private readonly IEmailSender _emailSender;


        public ClassRoomsControllerTest(ApplicationDbContextFixture contextFixture)
        {
            _fakeDb = new FakeDb(contextFixture);

            var _context = _fakeDb.GetContext();

            var users = _context.Users;

            var fakeUserManager = new Mock<FakeUserManager>();
            fakeUserManager.Setup(x => x.Users).Returns(users);
            fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<Person>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<Person>())).ReturnsAsync(IdentityResult.Success);


            _controller = new ClassRoomsController(_context, _emailSender);
        }
       

        [Fact]
        public async Task Index_returnsViewResultAsync()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<ClassRoom>>(viewResult.ViewData.Model).Where(u => u.isDeleted == false && u.SchoolName == "IPS - ESTSetúbal");
            Assert.NotNull(model);

        }
       
        [Fact]
        public async Task Details_GivenId_ReturnsViewResult()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = await _controller.Details(1);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void InitialCreate_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fc8fce04-c029-4416-b5dd-7cc7198deb40"),
                new Claim(ClaimTypes.Name, "joao.ratao@ejaf.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.InitialCreate();
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void InitialCreate_GivenClassRoom_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fc8fce04-c029-4416-b5dd-7cc7198deb40"),
                new Claim(ClaimTypes.Name, "joao.ratao@ejaf.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.InitialCreate(_fakeDb.GetContext().ClassRoom.FirstOrDefault(e => e.ClassRoomId == 2));
            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void GetClassesCreate_GivenEvaluation_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fc8fce04-c029-4416-b5dd-7cc7198deb40"),
                new Claim(ClaimTypes.Name, "joao.ratao@ejaf.pt"),
                new Claim(ClaimTypes.Role, "ClassDirector")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.GetClassesCreate(_fakeDb.GetContext().ClassRoom.FirstOrDefault(e => e.ClassRoomId == 2));
            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void Create_GivenCourseClass_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Create(1, 1);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void Create_GivenClassRoomOP_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            ClassRoomOP op = new ClassRoomOP();
            op.ClassRoom = _fakeDb.GetContext().ClassRoom.FirstOrDefault(c => c.ClassRoomId == 5);
            op.ListStudentPlaces = null;

            var result = _controller.Create(op);

            Assert.IsType<Task<IActionResult>>(result);
        }
       
        [Fact]
        public void Edit_ReturnsView()
        {
            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "fb2d77b5-5591-4a6b-ad82-e0164ed66350"),
                new Claim(ClaimTypes.Name, "admin@ejaf.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            var result = _controller.Edit(2);

            Assert.IsType<Task<IActionResult>>(result);
        }

        [Fact]
        public async Task Edit_GivenIdOP_ReturnsNotFound()
        {

            var logged = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "ee3d5706-9cd7-430c-ac81-7b377ec7d31d"),
                new Claim(ClaimTypes.Name, "admin@ips.pt"),
                new Claim(ClaimTypes.Role, "Admin")
            }));

            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = logged
                }
            };

            ClassRoomOP op = new ClassRoomOP();
            op.ClassRoom = _fakeDb.GetContext().ClassRoom.FirstOrDefault(c => c.ClassRoomId == 2);
            op.ListStudentPlaces = null;


            var result = await _controller.Edit(1, op);

            Assert.IsType<NotFoundResult>(result);
        }
    }
}
